/**
 * @file ListenSocket.h
 * @brief Definition of the ListenSocket class.
 *
 * @author Brian DeVries
 * @author David Bronke
 *
 * This header defines the ListenSocket class, which represents a host-side socket.
 */

#ifndef __RaptorNL__ListenSocket_h__
#define __RaptorNL__ListenSocket_h__

#include "Socket.h"

namespace RaptorNL
{
	/**
	 * @brief A host-side socket.
	 */
	class ListenSocket 
		:	public Socket
	{
	public:
		/**
		 * @brief Construct a new ListenSocket object.
		 *
		 * Binds the new socket to the given port, and listens for connections.
		 */
		ListenSocket (uint16_t port, SocketType type);


		/**
		 * @brief Destructor.
		 */
		~ListenSocket ();


		/**
		 * @brief Accept an incoming connection.
		 */
		Socket* acceptConnection ();
	}; // end class ListenSocket
} // end namespace RaptorNL

#endif // __RaptorNL_ListenSocket_h__

