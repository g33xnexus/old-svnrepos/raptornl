/**
 * @file Socket.h
 * @brief Definition of the Socket class.
 *
 * @author Brian DeVries
 * @author David Bronke
 *
 * This header defines the Socket class, which is used for all communication in
 * RaptorNL.
 */

#ifndef __RaptorNL__Socket_h__
#define __RaptorNL__Socket_h__

#include "raptornl.h"


namespace RaptorNL
{
	class Buffer;

	/**
	 * @brief A general network socket.
	 */
	class Socket
	{
	public:
		/**
		 * @brief Construct a new Socket object.
		 */
		Socket ();
		
		
		/**
		 * @brief Destructor.
		 */
		virtual ~Socket ();


		/**
		 * @brief Construct a Socket object for the given socket descriptor.
		 */
		Socket (NLsocket socket);


		/**
		 * @brief Accessor for the socket value.
		 *
		 * SHOULD NOT BE USED IN PRODUCTION CODE!!!
		 */
		NLsocket getSocket () const;


		/**
		 * @brief Open this socket.
		 *
		 * @return true if the socket was successfully opened, false otherwise.
		 */
		bool open (uint16_t port, SocketType type);


		/**
		 * @brief Close this socket.
		 *
		 * @return false on error, true otherwise.
		 */
		bool close ();


		/**
		 * @brief Test if this socket is valid.
		 *
		 * @return true if this socket is valid, false otherwise.
		 */
		bool isValid ();


		/**
		 * @brief Read from this socket into the given buffer.
		 *
		 * @param buffer the buffer to read data into
		 * @param nbytes the number of bytes of data to read
		 *
		 * @return the number of bytes read, or NL_INVALID on error.
		 */
		size_t read (void* buffer, size_t nbytes);


		/**
		 * @brief Read a Buffer from this socket.
		 *
		 * @return a new Buffer containing all read data.
		 */
		Buffer* read ();


		/**
		 * @brief Write from the buffer to the socket.
		 *
		 * @param buffer the buffer to write data from
		 * @param nbytes the number of bytes of data to write
		 *
		 * @return the number of bytes written, or NL_INVALID on error.
		 */
		size_t write (const void* buffer, size_t nbytes);


		/**
		 * @brief Write the contents of a Buffer to this socket.
		 *
		 * @param buffer the buffer to write data from
		 */
		void write (const Buffer* buffer);


		/**
		 * @brief Get the packets sent since the last clearPacketsSent ().
		 */
		int64_t packetsSent ();


		/**
		 * @brief Clear the number of packets sent.
		 *
		 * @return false if an error occurred, true otherwise.
		 */
		bool clearPacketsSent ();


		/**
		 * @brief Get the number of bytes sent since the last clearBytesSent ().
		 */
		int64_t bytesSent ();


		/**
		 * @brief Clear the number of bytes sent.
		 *
		 * @return false if an error occurred, true otherwise.
		 */
		bool clearBytesSent ();


		/**
		 * @brief Get the average bytes sent for the last 8 seconds.
		 */
		int64_t averageBytesSent ();


		/**
		 * @brief Reset the average number of bytes sent.
		 *
		 * @return false if an error occurred, true otherwise.
		 */
		bool clearAverageBytesSent ();


		/**
		 * @brief Get the highest number of bytes sent per second.
		 */
		int64_t highBytesSent ();


		/**
		 * @brief Clear the record of the highest number of bytes sent in one second.
		 *
		 * @return false if an error occurred, true otherwise.
		 */
		bool clearHighBytesSent ();


		/**
		 * @brief Return the number of packets received since the last clearPacketsReceived ().
		 */
		int64_t packetsReceived ();


		/**
		 * @brief Clear the number of packets received.
		 *
		 * @return false if an error occurred, true otherwise.
		 */
		bool clearPacketsReceived ();


		/**
		 * @brief Get the number of bytes received since the last clearBytesReceived ().
		 */
		int64_t bytesReceived ();


		/**
		 * @brief Clear the number of bytes received.
		 *
		 * @return false if an error occurred, true otherwise.
		 */
		bool clearBytesReceived ();


		/**
		 * @brief Get the average bytes received for the last 8 seconds.
		 */
		int64_t averageBytesReceived ();


		/**
		 * @brief Reset the average number of bytes received.
		 *
		 * @return false if an error occurred, true otherwise.
		 */
		bool clearAverageBytesReceived ();


		/**
		 * @brief Get the highest number of bytes received in one second.
		 */
		int64_t highestBytesReceived ();


		/**
		 * @brief Clear the number highest number of bytes received in one second.
		 *
		 * @return false if an error occurred, true otherwise.
		 */
		bool clearHighestBytesReceived ();


	protected:	// Types
		/**
		 * @brief Statistics about a given socket
		 */
		enum SocketStats
		{
			PACKETS_SENT	= 0x0030,	//< Total packets sent since last nlClear ()
			BYTES_SENT,					//< Total bytes sent since last nlClear
			AVE_BYTES_SENT,				//< Average bytes sent per second for the last 8 seconds
			HIGH_BYTES_SENT,			//< Highest bytes per second ever sent
			PACKETS_RECEIVED,			//< Total packets received since last nlClear
			BYTES_RECEIVED,				//< Total bytes received since last nlClear
			AVE_BYTES_RECEIVED,			//< Average bytes received per second for the last 8 seconds
			HIGH_BYTES_RECEIVED			//< Highest bytes per second ever received
		}; // end enum SocketStats


	protected:	// Data
		/// The socket we're dealing with
		NLsocket socket_;
	}; // end class Socket

	/*
	  Low level API, a thin layer over Sockets or other network provider.

	  Wrapped in an anonymous namespace to keep people from directly accessing it.

	  I have noted "done" or "TODO" on the entries to note which ones have been put into the above
	  Socket class and which still need to be put in there.
	*/
	namespace __PRIVATE__
	{
		// done
		HL_EXP bool HL_APIENTRY nlListen(NLsocket socket);

		// done
		HL_EXP NLsocket HL_APIENTRY nlAcceptConnection(NLsocket socket);

		// done
		HL_EXP NLsocket HL_APIENTRY nlOpen(uint16_t port, SocketType type);

		// done
		/// @TODO Fix Address
		HL_EXP bool HL_APIENTRY nlConnect(NLsocket socket, const NLaddress *address);

		// done
		HL_EXP bool HL_APIENTRY nlClose(NLsocket socket);

		// done
		HL_EXP int HL_APIENTRY nlRead(NLsocket socket, /*@out@*/ void *buffer, int nbytes);

		// done
		HL_EXP int HL_APIENTRY nlWrite(NLsocket socket, const void *buffer, int nbytes);

		// done
		/// @ingroup StatisticsAPI
		HL_EXP int64_t HL_APIENTRY nlGetSocketStat(NLsocket socket, int name);

		// done
		/// @ingroup StatisticsAPI
		HL_EXP bool HL_APIENTRY nlClearSocketStat(NLsocket socket, int name);

		/// @TODO
		/// @ingroup StatisticsAPI
		HL_EXP bool HL_APIENTRY nlPollSocket(NLsocket socket, int name, int timeout);

		/// @TODO
		/// @ingroup NetworkOptions
		HL_EXP bool HL_APIENTRY nlSetSocketOpt(NLsocket socket, int name, int arg);

		/// @TODO
		/// @ingroup NetworkOptions
		HL_EXP int HL_APIENTRY nlGetSocketOpt(NLsocket socket, int name);
	} // end namespace __PRIVATE__
} // end namespace RaptorNL

#endif // __RaptorNL__Socket_h__
