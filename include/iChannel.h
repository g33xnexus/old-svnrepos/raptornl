/**
 * @file iChannel.h
 * @brief Definition of the iChannel interface.
 *
 * @author David Bronke
 *
 * This header defines the iChannel interface.
 */

#ifndef __RaptorNL__iChannel_h__
#define __RaptorNL__iChannel_h__

#include "raptornl.h"


namespace RaptorNL
{
	class Buffer;

	/**
	 * @brief A communication channel.
	 */
	struct iChannel
	{
		/**
		 * @brief Determine whether this channel is reliable or not.
		 */
		virtual bool isReliable () = 0;


		/**
		 * @brief Determine whether this channel is ordered or not.
		 */
		virtual bool isOrdered () = 0;


		/**
		 * @brief Get the name of this channel.
		 */
		virtual string getName () = 0;


		/**
		 * @brief Get the ID of this channel.
		 */
		virtual int8_t getID () = 0;


		/**
		 * @brief Read from this socket into the given buffer.
		 *
		 * @param buffer the buffer to read data into
		 * @param nbytes the number of bytes of data to read
		 *
		 * @return the number of bytes read, or NL_INVALID on error.
		 */
		virtual size_t read (void* buffer, size_t nbytes) = 0;


		/**
		 * @brief Read a Buffer from this channel.
		 *
		 * @return a new Buffer containing all read data.
		 */
		virtual Buffer* read () = 0;


		/**
		 * @brief Write from the buffer to the socket.
		 *
		 * @param buffer the buffer to write data from
		 * @param nbytes the number of bytes of data to write
		 *
		 * @return the number of bytes written, or NL_INVALID on error.
		 */
		virtual size_t write (const void* buffer, size_t nbytes) = 0;


		/**
		 * @brief Write the contents of a Buffer to this socket.
		 *
		 * @param buffer the buffer to write data from
		 */
		virtual void write (const Buffer* buffer) = 0;
	}; // end class iChannel
} // end namespace RaptorNL

#endif // __RaptorNL__iChannel_h__

