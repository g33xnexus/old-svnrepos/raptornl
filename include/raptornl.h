/**
 * @file raptornl.h
 * @brief The master include file for RaptorNL.
 *
 * @author Brian DeVries
 * @author David Bronke
 * @author Phil Frisbie, Jr.
 */

/*
 Original HawkNL license blurb:
  HawkNL cross platform network library
  Copyright (C) 2000-2004 Phil Frisbie, Jr. (phil@hawksoft.com)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Library General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Library General Public License for more details.

  You should have received a copy of the GNU Library General Public
  License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
  Boston, MA  02111-1307, USA.

  Or go to http://www.gnu.org/copyleft/lgpl.html
*/

#ifndef __raptornl_h__
#define __raptornl_h__

#include <string>
#include <boost/cstdint.hpp>

#include "hawklib.h"


/* many CE devices will not allow non-aligned memory access */
#if defined (_WIN32_WCE)
#	define NL_SAFE_COPY
#	undef NL_INCLUDE_IPX
#endif


/* Detect whether this is a big endian or little endian system. */
/* @FIXME Ensure that this actually detects big vs. little endian correctly. Currently it doesn't. */
#if defined WIN32 || defined WIN64 || defined __i386__ || defined __alpha__ || defined __mips__
#	define NL_LITTLE_ENDIAN
#else
#	define NL_BIG_ENDIAN
#endif


/**
 * @brief The max string size.
 *
 * Limited to 256 (255 plus NULL termination) for MacOS.
 */
#define NL_MAX_STRING_LENGTH	256

/**
 * @brief The max packet size for NL_UDP* and NL_TCP_PACKETS.
 */
#define NL_MAX_PACKET_LENGTH	16384

/**
 * @brief The max number groups and sockets per group.
 */
#define NL_MAX_GROUPS		   128

#if defined (macintosh)
	/**
	 * @brief The maximum number of sockets per group.
	 *
	 * Does NOT apply to Mac OSX.
	 * @warning Macs only allow up to 32K of local data, don't exceed 4096.
	 */
#	define NL_MAX_GROUP_SOCKETS 4096
#else
	/**
	 * @brief The maximum number of sockets per group.
	 */
#	define NL_MAX_GROUP_SOCKETS 8192
#endif

/**
 * @brief Generic "invalid" value.
 */
#define NL_INVALID			  (-1)

#undef HL_EXP
/**
 * @brief Define HL_EXP as nothing.
 *
 * @TODO Why? What the hell is this?
 */
#define HL_EXP


/**
 * @brief The RaptorNL API namespace.
 */
namespace RaptorNL
{
	////////////////////////////////////////////////////////////////////////
	/**
	 * @defgroup Socket General Socket API
	 * @{
	 */

	/**
	 * @brief HawkNL socket type
	 */
	typedef int32_t NLsocket;


	/**
	 * @brief HawkNL enum type
	 */
	typedef int NLenum;


	/* NOTE: NLchar is only to be used for external strings that might be unicode. (?)
	 */
#	if defined _UNICODE
		typedef wchar_t NLchar;
		typedef std::wstring string;
#	else
		typedef char NLchar;
		typedef std::string string;
#	endif


	/**
	 * @brief HawkNL address struct
	 */
	struct NLaddress
	{
		uint8_t addr[32];	///< large enough to hold IPv6 address
		int driver;		///< driver type
		bool valid;		///< set to NL_TRUE when address is valid
	}; // end struct NLaddress


	/**
	 * @brief The available network types.
	 *
	 * Only one can be selected at a time.
	 */
	enum NetworkType
	{
		IP = 0x0003,	///< IPv4 address family (all platforms)
		IPV6,		///< IPv6 address family (all platforms) @TODO Not yet implemented
		LOOP_BACK,	///< Local loopback socket (no network) (all platforms)
		IPX,		///< Novell NetWare IPX protocol (Windows only)
		SERIAL,		///< Serial port @TODO implement
		MODEM,		///< Modem @TODO implement
		PARALLEL	///< Parallel port @TODO implement
	}; // end enum NetworkType


	/**
	 * @brief Socket types
	 */
	enum SocketType
	{
		TCP = 0x0010,	///< supported by IP (TCP), IPX (SPX), LOOP_BACK
		TCP_PACKETS,	///< supported by IP (TCP), IPX (SPX), LOOP_BACK
		UDP,		///< supported by IP (UDP), IPX, LOOP_BACK
		UDP_BROADCAST,	///< supported by IP (UDP), IPX, LOOP_BACK
		UDP_MULTICAST,	///< supported by IP (UDP)
		RAW,		///< supported by SERIAL or PARALLEL
		UDP_ENHANCED	///< supported by IP (UDP), LOOP_BACK
	}; // end enum SocketType


	/**
	 * @brief Standard multicast TTL settings as recommended by the white paper at
	 * http://www.ipmulticast.com/community/whitepapers/howipmcworks.html
	 */
	enum DefaultTTLSettings
	{
		LOCAL	= 1,	///< Local LAN only
		SITE	= 15,	///< This site
		REGION	= 63,	///< This region
		WORLD	= 127	///< The world
	}; // end enum DefaultTTLSettings


	namespace __PRIVATE__
	{
		/**
		 * @brief Initialize the network layer.
		 */
		HL_EXP bool HL_APIENTRY nlInit ();


		/**
		 * @brief Shut down the network layer.
		 */
		HL_EXP void HL_APIENTRY nlShutdown ();


		/**
		 * @brief Select the network type.
		 */
		HL_EXP bool HL_APIENTRY nlSelectNetwork (NetworkType network);
	}

	// @}



	////////////////////////////////////////////////////////////////////////
	/**
	 * @defgroup GroupAPI Socket Group API
	 * @{
	 */

	/**
	 * @brief Status for socket groups
	 */
	enum GroupStatus
	{
		NL_READ_STATUS = 0x0050, ///< Poll the read status for all sockets in the group.
		NL_WRITE_STATUS,		 ///< Poll the write status for all sockets in the group.
		NL_ERROR_STATUS		  ///< Poll the error status for all sockets in the group.

	}; // end enum GroupStatus


	namespace __PRIVATE__
	{
		/**
		 * @brief Create a socket group.
		 */
		HL_EXP int32_t HL_APIENTRY nlGroupCreate ();


		/**
		 * @brief Destroy a group.
		 */
		HL_EXP bool HL_APIENTRY nlGroupDestroy (int32_t group);


		/**
		 * @brief Add a socket to the given group.
		 */
		HL_EXP bool HL_APIENTRY nlGroupAddSocket (int32_t group, NLsocket socket);


		/**
		 * @brief Get the list of sockets in the given group.
		 */
		HL_EXP bool HL_APIENTRY nlGroupGetSockets (int32_t group, /*@out@*/ NLsocket *sockets, /*@in@*/ int *number);


		/**
		 * @brief Remove a socket from the given group.
		 */
		HL_EXP bool HL_APIENTRY nlGroupDeleteSocket (int32_t group, NLsocket socket);

		/**
		 * @brief Poll information from the given group.
		 */
		HL_EXP int32_t HL_APIENTRY nlPollGroup (int32_t group, GroupStatus name, /*@out@*/ NLsocket *sockets, int32_t number, int32_t timeout);
	}

	// @}



	////////////////////////////////////////////////////////////////////////
	/**
	 * @defgroup TimeAPI Time API
	 * @{
	 */

	/**
	 * @brief HawkNL time struct
	 */
	struct NLtime
	{
		int32_t seconds;	///< seconds since 12:00AM, 1 January, 1970
		int32_t mseconds;	///< milliseconds added to the seconds
		int32_t useconds;	///< microseconds added to the seconds
	}; // end struct NLtime


	namespace __PRIVATE__
	{
		/**
		 * @brief Get the current time.
		 *
		 * @param[out] ts the time structure to update
		 */
		HL_EXP bool HL_APIENTRY nlTime (NLtime *ts);
	}

	// @}



	////////////////////////////////////////////////////////////////////////
	/**
	 * @defgroup StatisticsAPI Statistics API
	 * @{
	 */

	/**
	 * @brief Used with nlGetString () to determine the type of message to send
	 */
	enum GetStringMsg
	{
		VERSION			= 0x0020,	///< The version string.
		NETWORK_TYPES,				///< Space delimited list of available network types.
		SOCKET_TYPES				///< Space delimited list of available socket types.
									///< Only valid AFTER nlSelectNetwork.
	}; // end enum GetStringMsg


	/**
	 * @brief Statistics
	 */
	enum Statistics
	{
		PACKETS_SENT	= 0x0030,	///< Total packets sent since last nlClear ()
		BYTES_SENT,					///< Total bytes sent since last nlClear
		AVE_BYTES_SENT,				///< Average bytes sent per second for the last 8 seconds
		HIGH_BYTES_SENT,			///< Highest bytes per second ever sent
		PACKETS_RECEIVED,			///< Total packets received since last nlClear
		BYTES_RECEIVED,				///< Total bytes received since last nlClear
		AVE_BYTES_RECEIVED,			///< Average bytes received per second for the last 8 seconds
		HIGH_BYTES_RECEIVED,		///< Highest bytes per second ever received
		ALL_STATS,					///< nlClear only, clears out all counters
		OPEN_SOCKETS				///< Number of open sockets
	}; // end enum Statistics


	namespace __PRIVATE__
	{
		/**
		 * @brief Retrieve the property specified by \p name.
		 */
		HL_EXP const /*@observer@*/ /*@null@*/ NLchar* HL_APIENTRY nlGetString (GetStringMsg name);


		/**
		 * @brief Retrieve the property specified by \p name.
		 */
		HL_EXP int32_t HL_APIENTRY nlGetInteger (Statistics name);


		/**
		 * @brief Clear the property specified by \p name.
		 */
		HL_EXP bool HL_APIENTRY nlClear (Statistics name);
	}

	// @}



	////////////////////////////////////////////////////////////////////////
	/**
	 * @defgroup ErrorHandling Error-handling API
	 * @{
	 */

	/**
	 * @brief Error codes
	 */
	enum Error
	{
		NO_ERROR			= 0x0000,	///< No error
		NO_NETWORK			= 0x0100,	///< No network was found on init or no network is selected
		OUT_OF_MEMORY,					///< out of memory

		INVALID_ENUM,					///< function called with an invalid NLenum
		INVALID_SOCKET,					///< socket is not valid, or has been terminated
		INVALID_PORT,					///< the port could not be opened
		INVALID_TYPE,					///< the network type is not available

		SYSTEM_ERROR,					///< a system error occurred, call nlGetSystemError
		SOCK_DISCONNECT,				///< the socket should be closed because of a connection loss or error
		NOT_LISTEN,						///< the socket has not been set to listen
		CON_REFUSED,					///< connection refused, or socket already connected

		NO_PENDING,						///< there are no pending connections to accept
		BAD_ADDR,						///< the address or port are not valid
		MESSAGE_END,					///< the end of a reliable stream (TCP) message has been reached
		NULL_POINTER,					///< a NULL pointer was passed to a function

		INVALID_GROUP,					///< the group is not valid, or has been destroyed
		OUT_OF_GROUPS,					///< out of internal group objects
		OUT_OF_GROUP_SOCKETS,			///< the group has no more room for sockets
		BUFFER_SIZE,					///< the buffer was too small to store the data, retry with a larger buffer

		PACKET_SIZE,					///< the size of the packet exceeds NL_MAX_PACKET_LENGTH or the protocol max
		WRONG_TYPE,						///< the function does not support the socket type
		CON_PENDING,					///< a non-blocking connection is still pending
		SELECT_NET_ERROR,				///< a network is already selected, and NL_MULTIPLE_DRIVERS is not enabled, call nlShutDown and nlInit first

		PACKET_SYNC,					///< the NL_RELIABLE_PACKET stream is out of sync
		TLS_ERROR,						///< thread local storage could not be created
		TIMED_OUT,						///< the function timed out
		SOCKET_NOT_FOUND,				///< the socket was not found in the group

		STRING_OVER_RUN,				///< the string is not null terminated, or is longer than NL_MAX_STRING_LENGTH
		CONNECTED						///< the socket is already connected
	}; // end enum Error

	namespace __PRIVATE__
	{
		/**
		 * @brief Return an error code corresponding to the last error.
		 */
		HL_EXP Error HL_APIENTRY nlGetError ();


		/**
		 * @brief Return an error description corresponding to the given error code.
		 */
		HL_EXP const /*@observer@*/ NLchar* HL_APIENTRY nlGetErrorStr (Error err);


		/**
		 * @brief Return an error code corresponding to the last system error.
		 */
		HL_EXP Error HL_APIENTRY nlGetSystemError ();


		/**
		 * @brief Return an error description corresponding to the given system error code.
		 */
		HL_EXP const /*@observer@*/ NLchar* HL_APIENTRY nlGetSystemErrorStr (Error err);
	}

	// @}



	////////////////////////////////////////////////////////////////////////
	/**
	 * @defgroup DataHashing Data hashing
	 * @{
	 */

	namespace __PRIVATE__
	{
		/**
		 * @brief Calculate the CRC16 hash of the given data block.
		 */
		HL_EXP uint16_t HL_APIENTRY nlGetCRC16 (uint8_t *data, size_t len);


		/**
		 * @brief Calculate the CRC32 hash of the given data block.
		 */
		HL_EXP uint32_t HL_APIENTRY nlGetCRC32 (uint8_t *data, size_t len);
	}

	// @}



	////////////////////////////////////////////////////////////////////////
	/**
	 * @defgroup NetworkOptions Networking options
	 * @{
	 */

	/**
	 * @brief Boolean networking options
	 */
	enum BoolOption
	{
		NL_BLOCKING_IO  = 0x0040, ///< Set IO to blocking. Default: NL_FALSE for non-blocking IO.
		NL_SOCKET_STATS,		  ///< Enable collection of socket read/write statistics. Default: disabled
		NL_BIG_ENDIAN_DATA,	   ///< Use big endian as the destination format for nlSwap*. Default: disabled
		NL_LITTLE_ENDIAN_DATA,	///< Use little endian as the destination format for nlSwap*. Default: disabled
		NL_MULTIPLE_DRIVERS,	  ///< Enable multiple drivers to be selected
		NL_TCP_NO_DELAY = 0x0063  ///< Disable the Nagle algorithm.
	}; // end enum BoolOption


	/**
	 * @brief Advanced network settings for experienced developers
	 */
	enum AdvancedSetting
	{
		NL_LISTEN_BACKLOG = 0x0060, ///< The backlog of connections for listen.
		NL_MULTICAST_TTL,		   ///< The multicast TTL value. Default: 1
		NL_REUSE_ADDRESS			///< Allow IP address to be reused. Default: NL_FALSE
	}; // end enum AdvancedSetting


	namespace __PRIVATE__
	{
		/**
		 * @brief Retrieve the option specified by \p name.
		 */
		HL_EXP bool HL_APIENTRY nlGetBoolean (BoolOption name);


		/**
		 * @brief Enable the given option.
		 */
		HL_EXP bool HL_APIENTRY nlEnable (BoolOption name);


		/**
		 * @brief Disable the given option.
		 */
		HL_EXP bool HL_APIENTRY nlDisable (BoolOption name);


		/**
		 * @brief Set the given network setting.
		 */
		HL_EXP bool HL_APIENTRY nlHint(AdvancedSetting name, int arg);
	}

	// @}



	////////////////////////////////////////////////////////////////////////
	/**
	 * @defgroup EndianFunctions Endian-correction functions
	 *
	 * The default destination byte order is big endian (network byte order),
	 * but it can be set to little endian.
	 * @{
	 */

	namespace __PRIVATE__
	{
		/**
		 * @brief Swap the byte order of the given uint16_t as needed.
		 *
		 * @return the swapped value.
		 */
		HL_EXP uint16_t HL_APIENTRY nlSwaps (uint16_t x);


		/**
		 * @brief Swap the byte order of the given uint32_t as needed.
		 *
		 * @return the swapped value.
		 */
		HL_EXP uint32_t HL_APIENTRY nlSwapl (uint32_t x);


		/**
		 * @brief Swap the byte order of the given uint64_t as needed.
		 *
		 * @return the swapped value.
		 */
		HL_EXP uint64_t HL_APIENTRY nlSwapll (uint64_t x);


		/**
		 * @brief Swap the byte order of the given float as needed.
		 *
		 * @return the swapped value.
		 */
		HL_EXP float HL_APIENTRY nlSwapf (float f);


		/**
		 * @brief Swap the byte order of the given double as needed.
		 *
		 * @return the swapped value.
		 */
		HL_EXP double HL_APIENTRY nlSwapd (double d);
	}

	// @}
	

	/**
	 * @brief A private namespace for encapsulating old HawkNL functions.
	 *
	 * <b>Do not</b> directly call anything in this namespace.
	 */
	namespace __PRIVATE__ {}
} // end namespace RaptorNL


#endif // __raptornl_h__

