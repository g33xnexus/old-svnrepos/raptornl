/**
 * @file iAddress.h
 * @brief Definition of the Address class.
 *
 * @author Brian DeVries
 * @author David Bronke
 *
 * This header defines the Address class, which represents a network address.
 */

#ifndef __RaptorNL__iAddress_h__
#define __RaptorNL__iAddress_h__

#include "raptornl.h"


namespace RaptorNL
{
	struct iSocket;

	/**
	 * @brief A network address.
	 *
	 * This class is a thin wrapper over the current HawkNL Address functionality.
	 */
	struct iAddress
	{
		/**
		 * @brief Convert this address to a string.
		 */
		virtual string toString () const = 0;


		/**
		 * Convert the given string to an address.
		 *
		 * @TODO Should this be here, or should this only be available
		 * in the constructor? Maybe make this part of the factory
		 * method for addresses in the iDriver or iSocket.
		 */
		virtual bool setAddress (string addressString) = 0;


		/**
		 * @brief Get the remote address from the given Socket.
		 *
		 * @TODO Move this into iSocket.
		 */
		virtual bool getRemoteAddr (const Socket& s) = 0;


		/**
		 * @brief Get the port number of this address.
		 */
		virtual uint16_t getPort () const = 0;


		/**
		 * @brief Set the port number of this address.
		 */
		virtual bool setPort (uint16_t port) = 0;


		/**
		 * @brief Attempt to resolve this address to a name.
		 *
		 * @TODO Make an asynchronous version of this?
		 */
		virtual string getName () const = 0;


		/**
		 * @brief Determine whether this address is valid.
		 *
		 * @return true if the address is valid, false otherwise.
		 */
		virtual bool isValid () const = 0;


		/**
		 * @brief Compare two addresses.
		 */
		virtual bool operator == (const iAddress& other) const = 0;


		/**
		 * @brief Assign this address to another.
		 */
		virtual Address& operator = (const iAddress& other) = 0;


		/**
		 * @brief Cast this address to a string.
		 */
		virtual operator const char* () const = 0;


		/**
		 * @brief Cast this address to a boolean.
		 *
		 * @return true if this address is valid, false otherwise.
		 */
		virtual operator bool () const = 0;
	}; // end class iAddress
} // end namespace RaptorNL

#endif // __RaptorNL__iAddress_h__
