/**
 * @file ConnectSocket.h
 * @brief Definition of the ConnectSocket class.
 *
 * @author Brian DeVries
 * @author David Bronke
 *
 * This header defines the ConnectSocket class, which represents a client-side socket.
 */

#ifndef __RaptorNL__ConnectSocket_h__
#define __RaptorNL__ConnectSocket_h__

#include "Socket.h"

namespace RaptorNL
{
	// Forward declaration
	class Address;

	/**
	 * @brief A client-side socket.
	 */
	class ConnectSocket
		:	public Socket
	{
	public:
		/**
		 * @brief Construct a new ConnectSocket object.
		 *
		 * Binds the new socket to the given port, and listens for connections.
		 */
		ConnectSocket (uint16_t port, SocketType type, Address* address);


		/**
		 * @brief Destructor.
		 */
		~ConnectSocket ();
	}; // end class ConnectSocket
} // end namespace RaptorNL

#endif // __RaptorNL_ConnectSocket_h__

