/**
 * @file iSocketGroup.h
 * @brief Definition of the iSocketGroup interface.
 *
 * @author David Bronke
 *
 * This header defines the iSocketGroup interface.
 */

#ifndef __RaptorNL__iSocketGroup_h__
#define __RaptorNL__iSocketGroup_h__

#include "raptornl.h"



namespace RaptorNL
{
	/**
	 * @brief An interface for a group of sockets.
	 *
	 * @TODO add a getChannel function?
	 * @TODO add a getChannelFactory function?
	 */
	class iSocketGroup
	{
	public:
		/**
		 * @brief Add a socket to this group.
		 */
		virtual void addSocket (iSocket* sock) = 0;


		/**
		 * @brief Remove a socket from this group.
		 */
		virtual void removeSocket (iSocket* sock) = 0;


		/**
		 * Poll various information from this group.
		 */
		virtual int32_t poll (NLenum name, /*@out@*/ NLsocket* sockets, int32_t number, int32_t timeout) = 0;
	}; // end class iSocketGroup
} // end namespace RaptorNL

#endif // __RaptorNL__iSocketGroup_h__

