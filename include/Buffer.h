/**
 * @file Buffer.h
 * @brief Definition of the Buffer class.
 *
 * @author Brian DeVries
 * @author David Bronke
 *
 * This header defines the Buffer class, which manages buffer reads and writes.
 */

#ifndef __RaptorNL__Buffer_h__
#define __RaptorNL__Buffer_h__

#include "raptornl.h"


namespace RaptorNL
{
	/**
	 * @brief A byte buffer.
	 *
	 * This class is a thin wrapper over the current HawkNL Buffer functionality,
	 * and will automatically endian swap the data as needed.
	 *
	 * @TODO Ensure that this works correctly on Solaris and WinCE devices. It
	 * may require the safe memcpy hack from HawkNL.
	 */
	class Buffer
	{
	public:
		class Iterator;


		/**
		 * @brief Construct a buffer.
		 */
		Buffer ();


		/**
		 * @brief Construct a buffer with the given contents.
		 */
		Buffer (const void* contents);


		/**
		 * @brief Construct a copy of the given buffer.
		 */
		Buffer (const Buffer& other);


		/**
		 * @brief Destructor.
		 */
		~Buffer ();


		/**
		 * @brief Write a int8_t to the buffer.
		 *
		 * @param data The int8_t to write
		 */
		void write (int8_t data);


		/**
		 * @brief Write a int16_t to the buffer.
		 *
		 * @param data The int16_t to write
		 */
		void write (int16_t data);


		/**
		 * @brief Write a int32_t to the buffer.
		 *
		 * @param data The int32_t to write
		 */
		void write (int32_t data);


		/**
		 * @brief Write a int64_t to the buffer.
		 *
		 * @param data The int64_t to write
		 */
		void write (int64_t data);


		/**
		 * @brief Write a uint8_t to the buffer.
		 *
		 * @param data The uint8_t to write
		 */
		void write (uint8_t data);


		/**
		 * @brief Write a uint16_t to the buffer.
		 *
		 * @param data The uint16_t to write
		 */
		void write (uint16_t data);


		/**
		 * @brief Write a uint32_t to the buffer.
		 *
		 * @param data The uint32_t to write
		 */
		void write (uint32_t data);


		/**
		 * @brief Write a uint64_t to the buffer.
		 *
		 * @param data The uint64_t to write
		 */
		void write (uint64_t data);


		/**
		 * @brief Write a float to the buffer.
		 *
		 * @param data The float to write
		 */
		void write (float data);


		/**
		 * @brief Write a double to the buffer.
		 *
		 * @param data The double to write
		 */
		void write (double data);


		/**
		 * @brief Write a string to the buffer.
		 *
		 * @param data The string to write
		 */
		void write (string data);


#ifdef _UNICODE
		/**
		 * @brief Write a wide-character string to the buffer.
		 *
		 * @param data The string to write
		 */
		void writeWCString (wchar_t* data);
#endif // _UNICODE


		/**
		 * @brief Write a block to the buffer.
		 *
		 * @param data The block to write
		 * @param length The length of the block
		 */
		void write (void* data, size_t length);


		/**
		 * @brief Calculate the CRC16 hash of this buffer.
		 */
		uint16_t getCRC16 ();


		/**
		 * @brief Calculate the CRC32 hash of this buffer.
		 */
		uint32_t getCRC32 ();


		/**
		 * @brief Get the size of this buffer, in bytes.
		 */
		size_t getSize () const;


		/**
		 * @brief Get an iterator over this buffer.
		 */
		Iterator getIterator () const;


		/**
		 * @brief An iterator over the contents of a Buffer.
		 */
		class Iterator
		{
		public:
			/**
			 * Copy constructor
			 */
			Iterator (const Iterator& other);


			/**
			 * Constructor
			 */
			Iterator (const string* buf);


			/**
			 * Destructor
			 */
			~Iterator ();


			/**
			 * @brief Retrieve an int8_t from the Buffer.
			 */
			int8_t getInt8 ();


			/**
			 * @brief Retrieve an uint8_t from the Buffer.
			 */
			uint8_t getUint8 ();


			/**
			 * @brief Retrieve an int16_t from the Buffer.
			 */
			int16_t getInt16 ();


			/**
			 * @brief Retrieve an uint16_t from the Buffer.
			 */
			uint16_t getUint16 ();


			/**
			 * @brief Retrieve an int32_t from the Buffer.
			 */
			int32_t getInt32 ();


			/**
			 * @brief Retrieve an uint32_t from the Buffer.
			 */
			uint32_t getUint32 ();


			/**
			 * @brief Retrieve an int64_t from the Buffer.
			 */
			int64_t getInt64 ();


			/**
			 * @brief Retrieve an uint64_t from the Buffer.
			 */
			uint64_t getUint64 ();


			/**
			 * @brief Retrieve a float from the Buffer.
			 */
			float getFloat ();


			/**
			 * @brief Retrieve a double from the Buffer.
			 */
			double getDouble ();


			/**
			 * @brief Retrieve a string from the Buffer.
			 */
			string getString ();


#ifdef _UNICODE
			/**
			 * @brief Read a string from the buffer.
			 *
			 * @return the string, or NULL if it could not be retrieved.
			 */
			wchar_t* getWCString ();
#endif // _UNICODE


			/**
			 * @brief Retrieve a block from the Buffer.
			 *
			 * @param length the length in bytes of the block to retrieve
			 */
			void* getBlock (size_t length);


			/**
			 * @brief Determine whether this iterator can still advance through the Buffer.
			 *
			 * @return true if the iterator has not yet reached the end of the buffer, false otherwise.
			 */
			bool hasNext ();


			/**
			 * Assign another iterator's position to this one.
			 */
			Iterator& operator = (const Iterator& other);


		private:
			const string* contents;
			size_t pos;
		}; // end class iterator


	private:
		string buf;
	}; // end class Buffer
} // end namespace RaptorNL

#endif // __RaptorNL__Buffer_h__

