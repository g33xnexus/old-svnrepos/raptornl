/**
 * @file Address.h
 * @brief Definition of the Address class.
 *
 * @author Brian DeVries
 * @author David Bronke
 *
 * This header defines the Address class, which represents a network address.
 */

#ifndef __RaptorNL__Address_h__
#define __RaptorNL__Address_h__

#include "raptornl.h"


namespace RaptorNL
{
	class Socket;

	/**
	 * @brief A network address.
	 *
	 * This class is a thin wrapper over the current HawkNL Address functionality.
	 */
	class Address
	{
	public:
		/**
		 * @brief Construct an address.
		 */
		Address ();


		/**
		 * @brief Construct a copy of the given address.
		 */
		Address (const Address& other);


		/**
		 * @brief Construct an address from the given string.
		 *
		 * Looks up the given @a name, or if the @a name is already formatted
		 * as an IP address, it simply uses the given address.
		 */
		Address (const string& name);


		/**
		 * @brief Construct an address from the given address struct.
		 */
		Address (const NLaddress* address);


		/**
		 * @brief Destructor.
		 */
		~Address ();


		/**
		 * @brief Convert this address to a string.
		 */
		string toString () const;


		/**
		 * @brief Get the remote address from the given Socket.
		 */
		bool getRemoteAddr (const Socket& s);


		/**
		 * @brief Get the port number of this address.
		 */
		uint16_t getPort () const;


		/**
		 * @brief Set the port number of this address.
		 */
		bool setPort (uint16_t port);


		/**
		 * @brief Get the underlying NLaddress struct in this object.
		 *
		 * SHOULD NOT BE USED IN PRODUCTION CODE!!!
		 */
		const NLaddress* getAddress () const;


		/**
		 * @brief Attempt to resolve this address to a name.
		 */
		string getName () const;


		/**
		 * @brief Determine whether this address is valid.
		 *
		 * @return true if the address is valid, false otherwise.
		 */
		bool isValid () const;


		/**
		 * @brief Compare two addresses.
		 */
		bool operator == (const Address& other) const;


		/**
		 * @brief Assign this address to another.
		 */
		Address& operator = (const Address& other);


		/**
		 * @brief Cast this address to a string.
		 */
		operator const char* () const;


		/**
		 * @brief Cast this address to a boolean.
		 *
		 * @return true if this address is valid, false otherwise.
		 */
		operator bool () const;


	private:	// data
		/// The internal address value
		NLaddress* address_;
	}; // end class Address


	/*
	 * Low level Address management API
	 * Wrapped in a "private" namespace to keep people from messing with it
	 */
	namespace __PRIVATE__
	{
		/// @TODO move these into Socket instead?
			// done
			HL_EXP bool HL_APIENTRY nlGetRemoteAddr(NLsocket socket, /*@out@*/ NLaddress *address);

			/// @TODO
			HL_EXP bool HL_APIENTRY nlSetRemoteAddr(NLsocket socket, const NLaddress *address);

			/// @TODO
			HL_EXP bool HL_APIENTRY nlGetLocalAddr(NLsocket socket, /*@out@*/ NLaddress *address);

		/// @TODO move these into the base network layer?
			/// @TODO
			HL_EXP NLaddress* HL_APIENTRY nlGetAllLocalAddr(/*@out@*/ int *count);

			/// @TODO
			HL_EXP bool HL_APIENTRY nlSetLocalAddr(const NLaddress *address);

		/// @TODO do we want to support asynchronous name resolution, and if so, how?
			/// @TODO
			HL_EXP bool HL_APIENTRY nlGetNameFromAddrAsync(const NLaddress *address, /*@out@*/ NLchar *name);

			/// @TODO
			HL_EXP bool HL_APIENTRY nlGetAddrFromNameAsync(const NLchar *name, /*@out@*/ NLaddress *address);

		// done
		HL_EXP /*@null@*/ NLchar* HL_APIENTRY nlAddrToString(const NLaddress *address, /*@returned@*/ /*@out@*/ NLchar *string);

		// done (covered by nlGetAddrFromName)
		HL_EXP bool HL_APIENTRY nlStringToAddr(const NLchar *string, /*@out@*/ NLaddress *address);

		// done
		HL_EXP /*@null@*/ NLchar* HL_APIENTRY nlGetNameFromAddr(const NLaddress *address, /*@returned@*/ /*@out@*/ NLchar *name);

		// done
		HL_EXP bool HL_APIENTRY nlGetAddrFromName(const NLchar *name, /*@out@*/ NLaddress *address);

		// done
		HL_EXP bool HL_APIENTRY nlAddrCompare(const NLaddress *address1, const NLaddress *address2);

		// done
		HL_EXP uint16_t HL_APIENTRY nlGetPortFromAddr(const NLaddress *address);

		// done
		HL_EXP bool HL_APIENTRY nlSetAddrPort(NLaddress *address, uint16_t port);
	} // end namespace __PRIVATE__
} // end namespace RaptorNL

#endif // __RaptorNL__Address_h__
