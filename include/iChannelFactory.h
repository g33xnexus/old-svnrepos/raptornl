/**
 * @file iChannelFactory.h
 * @brief Definition of the iChannelFactory interface.
 *
 * @author David Bronke
 *
 * This header defines the iChannelFactory interface.
 */

#ifndef __RaptorNL__iChannelFactory_h__
#define __RaptorNL__iChannelFactory_h__

#include "raptornl.h"


namespace RaptorNL
{
	struct iChannel;

	/**
	 * @brief A general network socket.
	 */
	struct iChannelFactory
	{
		/**
		 * @brief Get the ID of this channel.
		 */
		virtual iChannel* createChannel (string name, bool reliable, bool ordered) = 0;
	}; // end class iChannelFactory
} // end namespace RaptorNL

#endif // __RaptorNL__iChannelFactory_h__

