/**
 * @file iDriver.h
 * @brief Definition of the Driver class.
 *
 * @author David Bronke
 *
 * This header defines the Driver class, which represents a network driver.
 */

#ifndef __RaptorNL__iDriver_h__
#define __RaptorNL__iDriver_h__

#include "raptornl.h"

#include <vector>


namespace RaptorNL
{
	/**
	 * @brief The class representing a network driver.
	 *
	 * @TODO Much of this should really be part of the implementation of
	 * Socket, Address, and Group. There shouldn't be much to put in the
	 * Driver class except initialization, shutdown, and factories for
	 * Sockets, Addresses, and Groups.
	 */
	struct iDriver
	{
		/**
		 * Get the name of this network driver.
		 */
		virtual const string getName () const = 0;


		/**
		 * Get the type of this network driver.
		 */
		virtual const NetworkType getNetworkType () const = 0;


		/**
		 * Get the types of sockets supported by this network driver.
		 */
		virtual const std::vector<NetworkType> getSocketTypes () const = 0;


		/**
		 * Open a new socket.
		 *
		 * @param port the port to attach the socket to.
		 * @param type the type of socket to open.
		 */
		virtual iSocket* open (uint16_t port, SocketType type) = 0;


		/**
		 * Get the local addresses of all open sockets.
		 *
		 * @TODO Do we really need this?
		 */
		virtual std::vector<iAddress> getAllLocalAddr () const = 0;


		/**
		 * Set an advanced option or parameter or thingy.
		 * @TODO Come up with a better name. This one sucks. Maybe even come up with a more generic way to set options.
		 */
		virtual bool hint (NLenum name, int32_t arg) = 0;


		/**
		 * Set an advanced option for the given socket.
		 *
		 * @TODO Put this in iSocket. It shouldn't be here.
		 */
		virtual bool setSocketOpt (NLsocket socket, NLenum name, int32_t arg) = 0;


		/**
		 * Get the value of and advanced option for the given socket.
		 *
		 * @TODO Put this in iSocket. It shouldn't be here.
		 */
		virtual int32_t getSocketOpt (NLsocket socket, NLenum name) const = 0;
	}; // end class Driver
} // end namespace RaptorNL

#endif // __RaptorNL__iDriver_h__
