/**
 * @file iSocket.h
 * @brief Definition of the iSocket interface.
 *
 * @author David Bronke
 *
 * This header defines the iSocket interface.
 */

#ifndef __RaptorNL__iSocket_h__
#define __RaptorNL__iSocket_h__

#include "raptornl.h"



namespace RaptorNL
{
	/**
	 * @brief A general network socket interface.
	 *
	 * @TODO add a getChannel function?
	 * @TODO add a getChannelFactory function?
	 */
	class iSocket
	{
	public:
		/**
		 * @brief Open this socket.
		 *
		 * @return true if the socket was successfully opened, false otherwise.
		 */
		virtual bool open (uint16_t port, iSocketType type) = 0;


		/**
		 * @brief Close this socket.
		 *
		 * @return false on error, true otherwise.
		 */
		virtual bool close () = 0;


		/**
		 * @brief Test if this socket is valid.
		 *
		 * @return true if this socket is valid, false otherwise.
		 */
		virtual bool isValid () const = 0;


		/**
		 * Listen for connections on the given socket.
		 */
		virtual bool listen () = 0;


		/**
		 * Accept a connection on the given socket.
		 */
		virtual iSocket* acceptConnection () = 0;


		/**
		 * Connect the given socket to an address.
		 *
		 * @param socket the socket to connect.
		 * @param address the address to connect to.
		 */
		virtual bool connect (const iAddress* address) = 0;


		/**
		 * Close the given socket.
		 */
		virtual void close () = 0;


		/**
		 * Read from the given socket.
		 * @TODO Should this only be in iChannel?
		 */
		virtual int32_t read (/*@out@*/ void* buffer, int32_t nbytes) = 0;


		/**
		 * Write to the given socket.
		 * @TODO Should this only be in iChannel?
		 */
		virtual int32_t write (const void* buffer, int32_t nbytes) = 0;


		/**
		 * Get the local address of the given socket.
		 */
		virtual iAddress* getLocalAddr () const = 0;


		/**
		 * Set the local address.
		 */
		virtual bool setLocalAddr (const iAddress* address) = 0;


		/**
		 * Poll various information from this socket.
		 */
		virtual bool pollSocket (NLenum name, int32_t timeout) = 0;
	}; // end class iSocket
} // end namespace RaptorNL

#endif // __RaptorNL__iSocket_h__

