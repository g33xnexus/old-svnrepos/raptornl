////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file	simpChat.h
 * @author	Christopher S. Case
 *
 * @brief   Simple Chat program for RaptorNL.
 *
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __simpChat_h__
#define __simpChat_h__

#include "raptornl.h"
#include "Socket.h"
#include "Address.h"
#include <iostream>

using namespace RaptorNL;

////////////////////////////////////////////////////////////////////////////////
// Variables

/// The user's choice about if we're a server or not.
int servChoice;

/// The user's choice of IP address.
string addr;

/// The user's choice of port.
uint16_t port;

/// The socket.
Socket* socket;

////////////////////////////////////////////////////////////////////////////////
// Functions

/**
 * Report the most recent error.
 */
void ReportError ();

/**
 * Enters a loop were it recieves anything sent through the socket, and send anything typed by the user.
 */
bool SendRecieve (int choice);

#endif // __simpChat_h__

