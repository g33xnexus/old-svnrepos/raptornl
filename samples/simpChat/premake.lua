package.name = "simpChat"
package.kind = "exe"
package.language = "c++"
package.target = "simpChat"

package.libpaths = {}
package.includepaths = { "../../include" }
package.buildflags = { "optimize", "no-frame-pointer", "extra-warnings", "no-rtti" }
package.defines = { "_REENTRANT" }

if linux or bsd or macosx then --or solaris
	package.links = { "pthread" }
	if macosx then
		table.insert (package.defines, "MACOSX")
	else
		table.insert (package.defines, "_GNU_SOURCE")
--		TODO: Add Solaris support to premake.
--		if solaris then
--			table.insert (package.defines, "NL_SAFE_COPY")
--			table.insert (package.defines, "SOLARIS")
--			table.insert (package.links, "nsl")
--			table.insert (package.links, "socket")
--		end
	end
	package.buildoptions = { "-funroll-loops", "-ffast-math", "-Weffc++" }
else
	if windows then
		table.insert (package.defines, "WIN32")
		package.links = { "wsock32" }
		if options["with-pthread"] then
			table.insert (package.links, "pthread")
			if not options["disable-builtin-pthread"] then
				table.insert (package.libpaths, "pthread_win32")
				table.insert (package.includepaths, "pthread_win32")
			end
		end
		if options.target == "gnu" then
			package.buildoptions = { "-funroll-loops", "-ffast-math", "-Weffc++" }
		end
	end
end

table.insert (package.links, "src_old")
table.insert (package.links, "src")

table.insert (package.defines, "RNL_MAJOR_VERSION="..MAJOR_VERSION)
table.insert (package.defines, "RNL_MINOR_VERSION="..MINOR_VERSION)
table.insert (package.defines, "RNL_PATCH_VERSION="..PATCH_LEVEL)
table.insert (package.defines, "RNL_VERSION_STRING=\\\"RaptorNL "..MAJOR_VERSION.."."..MINOR_VERSION.."."..PATCH_LEVEL.."\\\"")

package.files = {
	"simpChat.cpp",
	"simpChat.h"
}
