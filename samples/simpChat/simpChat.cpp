////////////////////////////////////////////////////////////////////////////////
/**
 * Copyright (C) 2004-2006 G33X Nexus Entertainment (http://gne.g33xnexus.com/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/**
 * @file	simpChat.cpp
 * @author	Christopher S. Case
 *
 * @brief   Simple Chat program for RaptorNL.
 *
 */
////////////////////////////////////////////////////////////////////////////////


#ifndef __simpChat_cpp__
#define __simpChat_cpp__

#include "simpChat.h"
#include "ListenSocket.h"
#include "ConnectSocket.h"
#include "Address.h"
#include "Buffer.h"


////////////////////////////////////////////////////////////////////////////////
/**
 * Main loop. Everything exciting happens here.
 */
int main ()
{
	// Set the network and socket type to use.
	NetworkType netType = IP;
	SocketType sockType = TCP_PACKETS;

	// Display what program we are.
	std::cout << "Simple Chat v.01" << std::endl << "RaptorNL Sample Program" << std::endl;

	// Initialize the Network Layer.
	if (!__PRIVATE__::nlInit ())
	{
		std::cerr << "Couldn't initialize network layer!" << std::endl;
		ReportError ();
		exit (EXIT_FAILURE);
	}

	// Select the IP network type.
	if (!__PRIVATE__::nlSelectNetwork (netType))
	{
		std::cerr << "Couldn't select IP network type!" << std::endl;
		ReportError ();
		exit (EXIT_FAILURE);
	}

	// Ask the user if we're a server or not.
	std::cout << std::endl << "Please Select:" << std::endl << std::endl;
	std::cout << "  1: Server" << std::endl;
	std::cout << "  2: Client" << std::endl << std::endl << "# ";

	std::cin >> servChoice;

	// Decide what type of socket to open up.
	if (servChoice == 1)
	{
		// Ask for which port to listen on.
		std::cout << std::endl << "Port? # ";
		std::cin >> port;

		std::cout << std::endl << "Creating listen socket . . .";

		// Open a Listen Socket.
		ListenSocket* listen = new ListenSocket (port, sockType);

		// Test if the new socket is valid.
		if (!listen->isValid ())
		{
			std::cerr << "Couldn't open a listen socket!" << std::endl;
			ReportError ();
			delete listen;
			exit (EXIT_FAILURE);
		}

		// Wait for a connection.
		std::cout << std::endl << "Waiting for incoming connection . . .";
		while ((socket = listen->acceptConnection ()) == NULL);

		std::cout << std::endl << "Got connection!" << std::endl;

		// Close the listen socket. (we only want to accept one connection)
		listen->close ();
		delete listen;
	}
	else if (servChoice == 2)
	{
		// Ask for Address and port.
		std::cout << std::endl << "IP Address? # ";
		std::cin >> addr;

		std::cout << std::endl << "Port? # ";
		std::cin >> port;

		// Tell the user we're opening a socket.
		std::cout << std::endl << "Connecting to: " << addr << ":" << port << " . . ." << std::endl;

		/// Create an address object.
		Address Address (addr);
		Address.setPort (port);

		// Open a Connect Socket.
		socket = new ConnectSocket (port, sockType, &Address);

		// Test if the new socket is valid.
		if (!socket->isValid ())
		{
			std::cerr << "Couldn't open a connect socket!" << std::endl;
			ReportError ();
			delete socket;
			exit (EXIT_FAILURE);
		}

		// Tell the User we've finished connecting
		std::cout << "Finished connecting!" << std::endl;
	} // end if

	// Call function to send/recieve data data until '/quit' or quit message arrives.
	if (!SendRecieve (servChoice))
	{
		ReportError ();
		exit (EXIT_FAILURE);
	} // end if

	// We've finished successfully.
	exit (EXIT_SUCCESS);
} // end main ()

/**
 * Report the most recent error.
 */
void ReportError ()
{
	Error err = __PRIVATE__::nlGetError ();
	if (err == SYSTEM_ERROR)
	{
		std::cerr << "RaptorNL system error: " << __PRIVATE__::nlGetSystemErrorStr (__PRIVATE__::nlGetSystemError ()) << std::endl;
	}
	else if (err != NO_ERROR)
	{
		std::cerr << "RaptorNL error: " << __PRIVATE__::nlGetErrorStr (__PRIVATE__::nlGetError ()) << std::endl;
	} // end if
} // end ReportError ()

bool SendRecieve (int choice)
{
	Buffer* buff;
	bool quit = false;
	string stringVar, input;

	while (!quit)
	{
		// Read from the socket.
		buff = socket->read ();

		// Get our string out of the Buffer.
		if (buff != NULL)
		{
			std::cout << "Server Sends: " << std::endl << "	";

			Buffer::Iterator it = buff->getIterator();
			stringVar = it.getString();
			delete buff;

			if (stringVar == "/quit")
			{
				// The remote computer sent a quit message.
				quit = true;
				if (choice == 1)
				{
					std::cout << std::endl << std::endl << "Server Quit!" << std::endl;
				}
				else
				{
					std::cout << std::endl << std::endl << "Client Quit!" << std::endl;
				} // end if
			} // end if
		} // end if

		// Print out what was sent.
		std::cout << stringVar << std::endl;

		// Prompt user to input text
		std::cout << std::endl << "Input # ";

		std::cin >> input;

		if (input == "/quit")
		{
			// The local user sent a quit message.
			std::cout << std::endl << std::endl << "Exiting..." << std::endl;
			quit = true;
		}

		//Put the input into a Buffer.
		buff = new Buffer ();
		buff->write (input);

		// Write code:
		socket->write (buff);

		delete buff;
	} // end while

	return true;
} // end SendRecieve ()

#endif // __simpChat_cpp__
