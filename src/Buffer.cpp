#include "raptornl.h"
#include "Buffer.h"

//TODO: Do we need this?
//#ifdef _UNICODE
//#include <cstdlib>
//#endif // _UNICODE


namespace RaptorNL
{
	////////////////////////////////////////////////////////////////////////////
	Buffer::Buffer () :
		buf (NULL)
	{
	} // end Buffer ()


	////////////////////////////////////////////////////////////////////////////
	Buffer::Buffer (const void* contents) :
		buf ((char*) contents)
	{
	} // end Buffer ()


	////////////////////////////////////////////////////////////////////////////
	Buffer::Buffer (const Buffer& other) :
		buf (other.buf)
	{
	} // end Buffer ()


	////////////////////////////////////////////////////////////////////////////
	Buffer::~Buffer ()
	{
	} // end ~Buffer ()


	////////////////////////////////////////////////////////////////////////////
	void Buffer::write (int8_t data)
	{
		char temp[1];
		* ((int8_t *) temp) = data;
		buf.append (temp, 1);
	} // end write ()


	////////////////////////////////////////////////////////////////////////////
	void Buffer::write (int16_t data)
	{
		char temp[2];
		* ((int16_t *) ((int8_t *) temp)) = __PRIVATE__::nlSwaps (data);
		buf.append (temp, 2);
	} // end write ()


	////////////////////////////////////////////////////////////////////////////
	void Buffer::write (int32_t data)
	{
		char temp[4];
		* ((int32_t *) ((int8_t *) temp)) = __PRIVATE__::nlSwapl (data);
		buf.append (temp, 4);
	} // end write ()


	////////////////////////////////////////////////////////////////////////////
	void Buffer::write (int64_t data)
	{
		char temp[8];
		* ((int64_t *) ((int8_t *) temp)) = __PRIVATE__::nlSwapll (data);
		buf.append (temp, 8);
	} // end write ()


	////////////////////////////////////////////////////////////////////////////
	void Buffer::write (uint8_t data)
	{
		char temp[1];
		* ((uint8_t *) temp) = data;
		buf.append (temp, 1);
	} // end write ()


	////////////////////////////////////////////////////////////////////////////
	void Buffer::write (uint16_t data)
	{
		char temp[2];
		* ((uint16_t *) ((uint8_t *) temp)) = __PRIVATE__::nlSwaps (data);
		buf.append (temp, 2);
	} // end write ()


	////////////////////////////////////////////////////////////////////////////
	void Buffer::write (uint32_t data)
	{
		char temp[4];
		* ((uint32_t *) ((uint8_t *) temp)) = __PRIVATE__::nlSwapl (data);
		buf.append (temp, 4);
	} // end write ()


	////////////////////////////////////////////////////////////////////////////
	void Buffer::write (uint64_t data)
	{
		char temp[8];
		* ((uint64_t *) ((uint8_t *) temp)) = __PRIVATE__::nlSwapll (data);
		buf.append (temp, 8);
	} // end write ()


	////////////////////////////////////////////////////////////////////////////
	void Buffer::write (float data)
	{
		char temp[4];
		* ((float *) ((uint8_t *) temp)) = __PRIVATE__::nlSwapf (data);
		buf.append (temp, 4);
	} // end write ()


	////////////////////////////////////////////////////////////////////////////
	void Buffer::write (double data)
	{
		char temp[8];
		* ((double *) ((uint8_t *) temp)) = __PRIVATE__::nlSwapd (data);
		buf.append (temp, 8);
	} // end write ()


	////////////////////////////////////////////////////////////////////////////
	void Buffer::write (std::string data)
	{
		buf.append (data);
	} // end write ()


#ifdef _UNICODE
	////////////////////////////////////////////////////////////////////////////
	void Buffer::writeWCString (wchar_t* data)
	{
		size_t len = wcstombs (NULL, data, 0) + 1;
		char temp[len + 1];
		len = wcstombs (temp, data, len);

		if (len > 0)
		{
			buf.append (temp, len + 1);
		}
		else
		{
			/* there was an error in wcstombs, so just add a 0 length string to the buffer */
			buf.append ("\0");
		}
	} // end writeWCString ()
#endif // _UNICODE


	////////////////////////////////////////////////////////////////////////////
	void Buffer::write (void* data, size_t length)
	{
		char temp[sizeof (size_t)];
		buf.append (temp, sizeof (size_t));
		buf.append ((char *) data, length);
	} // end write ()


	////////////////////////////////////////////////////////////////////////////
	uint16_t Buffer::getCRC16 ()
	{
		int8_t* temp = NULL;
		strncpy ((char*) temp, buf.c_str (), buf.size ());
		return __PRIVATE__::nlGetCRC16 ((uint8_t*) temp, buf.size ());
	} // end getCRC16 ()


	////////////////////////////////////////////////////////////////////////////
	uint32_t Buffer::getCRC32 ()
	{
		int8_t* temp = NULL;
		strncpy ((char*) temp, buf.c_str (), buf.size ());
		return __PRIVATE__::nlGetCRC32 ((uint8_t*) temp, buf.size ());
	} // end getCRC32 ()

	////////////////////////////////////////////////////////////////////////////
	size_t Buffer::getSize () const
	{
		return buf.size ();
	} // end getSize ()

	////////////////////////////////////////////////////////////////////////////
	Buffer::Iterator Buffer::getIterator () const
	{
		return Iterator (&buf);
	}


#define GET_TYPE(swap, type, bytelen) \
		char temp[bytelen]; \
		contents->copy (temp, bytelen, pos); \
		pos += bytelen; \
		return swap ((type) (*temp));


	////////////////////////////////////////////////////////////////////////////
	Buffer::Iterator::Iterator (const Buffer::Iterator& other) :
		contents (other.contents),
		pos (other.pos)
	{
	} // end Iterator ()


	////////////////////////////////////////////////////////////////////////////
	Buffer::Iterator::Iterator (const std::string* buf) :
		contents (buf),
		pos (0)
	{
	} // end Iterator ()


	////////////////////////////////////////////////////////////////////////////
	Buffer::Iterator::~Iterator ()
	{
	} // end ~Iterator ()


	////////////////////////////////////////////////////////////////////////////
	int8_t Buffer::Iterator::getInt8 ()
	{
		return (int8_t) contents->at (pos++);
	} // end getInt8 ()


	////////////////////////////////////////////////////////////////////////////
	uint8_t Buffer::Iterator::getUint8 ()
	{
		return (uint8_t) contents->at (pos++);
	} // end getUint8 ()


	////////////////////////////////////////////////////////////////////////////
	int16_t Buffer::Iterator::getInt16 ()
	{
		GET_TYPE(__PRIVATE__::nlSwaps, int16_t, 2)
	} // end getInt16 ()


	////////////////////////////////////////////////////////////////////////////
	uint16_t Buffer::Iterator::getUint16 ()
	{
		GET_TYPE(__PRIVATE__::nlSwaps, uint16_t, 2)
	} // end getUint16 ()


	////////////////////////////////////////////////////////////////////////////
	int32_t Buffer::Iterator::getInt32 ()
	{
		GET_TYPE(__PRIVATE__::nlSwapl, int32_t, 4)
	} // end getInt32 ()


	////////////////////////////////////////////////////////////////////////////
	uint32_t Buffer::Iterator::getUint32 ()
	{
		GET_TYPE(__PRIVATE__::nlSwapl, uint32_t, 4)
	} // end getUint32 ()


	////////////////////////////////////////////////////////////////////////////
	int64_t Buffer::Iterator::getInt64 ()
	{
		GET_TYPE(__PRIVATE__::nlSwapll, int64_t, 8)
	} // end getInt64 ()


	////////////////////////////////////////////////////////////////////////////
	uint64_t Buffer::Iterator::getUint64 ()
	{
		GET_TYPE(__PRIVATE__::nlSwapll, uint64_t, 8)
	} // end getUint64 ()


	////////////////////////////////////////////////////////////////////////////
	float Buffer::Iterator::getFloat ()
	{
		GET_TYPE(__PRIVATE__::nlSwapf, float, 4)
	} // end getFloat ()


	////////////////////////////////////////////////////////////////////////////
	double Buffer::Iterator::getDouble ()
	{
		GET_TYPE(__PRIVATE__::nlSwapd, double, 4)
	} // end getDouble ()


	////////////////////////////////////////////////////////////////////////////
	std::string Buffer::Iterator::getString ()
	{
		size_t begin = pos;
		pos = contents->find ('\0', pos);
		return std::string (contents->substr (begin, pos - begin));
	} // end getString ()


#ifdef _UNICODE
	////////////////////////////////////////////////////////////////////////////
	wchar_t* Buffer::Iterator::getWCString ()
	{
		size_t begin = pos;
		pos = contents->find ('\0', pos);
		const char* data = contents->substr (begin, pos - begin).c_str ();
		size_t len = mbstowcs (NULL, data, 0) + 1;
		wchar_t* temp = new wchar_t[len + 1];
		len = mbstowcs (temp, data, len);

		if (len < 0)
		{
			delete temp;
			temp = NULL;
		}
		return temp;
	} // end getWCString ()
#endif // _UNICODE


	////////////////////////////////////////////////////////////////////////////
	void* Buffer::Iterator::getBlock (size_t length)
	{
		void* block = new char[length];
		memcpy (block, contents->substr (pos, length).c_str (), length);
		pos += length;
		return block;
	} // end getBlock ()


	////////////////////////////////////////////////////////////////////////////
	bool Buffer::Iterator::hasNext ()
	{
		return (pos != contents->size ());
	} // end getBlock ()


	////////////////////////////////////////////////////////////////////////////
	Buffer::Iterator& Buffer::Iterator::operator = (const Buffer::Iterator& other)
	{
		contents = other.contents;
		pos = other.pos;
		return *this;
	} // end operator = ()
} // end namespace RaptorNL

