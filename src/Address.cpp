#include "raptornl.h"
#include "Address.h"
#include "Socket.h"

namespace RaptorNL
{
	////////////////////////////////////////////////////////////////////////////
	Address::Address () :
		address_ (NULL)
	{
	} // end Address ()


	////////////////////////////////////////////////////////////////////////////
	Address::Address (const Address& other) :
		address_ (NULL)
	{
		memcpy (address_, other.getAddress (), sizeof (NLaddress));
	} // end Address ()


	////////////////////////////////////////////////////////////////////////////
	Address::Address (const std::string& name) :
		address_ (NULL)
	{
		if (!__PRIVATE__::nlGetAddrFromName (name.c_str (), address_))
		{
			address_ = NULL;
		}
	} // end Address ()


	////////////////////////////////////////////////////////////////////////////
	Address::Address (const NLaddress* address) :
		address_ (NULL)
	{
		memcpy (address_, address, sizeof (NLaddress));
	} // end Address ()


	////////////////////////////////////////////////////////////////////////////
	Address::~Address ()
	{
	} // end Address ()


	////////////////////////////////////////////////////////////////////////////
	std::string Address::toString () const
	{
		char* str = "";
		__PRIVATE__::nlAddrToString (address_, str);
		return std::string (str);
	} // end toString ()


	////////////////////////////////////////////////////////////////////////////
	bool Address::getRemoteAddr (const Socket& s)
	{
		return __PRIVATE__::nlGetRemoteAddr (s.getSocket (), address_);
	} // end getRemoteAddr ()


	////////////////////////////////////////////////////////////////////////////
	uint16_t Address::getPort () const
	{
		return __PRIVATE__::nlGetPortFromAddr (address_);
	} // end getPort ()


	////////////////////////////////////////////////////////////////////////////
	bool Address::setPort (uint16_t port)
	{
		return __PRIVATE__::nlSetAddrPort (address_, port);
	} // end setPort ()


	////////////////////////////////////////////////////////////////////////////
	const NLaddress* Address::getAddress () const
	{
		return address_;
	} // end getAddress ()


	////////////////////////////////////////////////////////////////////////////
	std::string Address::getName () const
	{
		char buffer[NL_MAX_STRING_LENGTH];
		return std::string (__PRIVATE__::nlGetNameFromAddr (address_, buffer));
	} // end getName ()


	////////////////////////////////////////////////////////////////////////////
	bool Address::isValid () const
	{
		return (address_ != NULL && address_->valid);
	} // end isValid ()


	////////////////////////////////////////////////////////////////////////////
	bool Address::operator == (const Address& other) const
	{
		return __PRIVATE__::nlAddrCompare (address_, other.getAddress ());
	} // end operator== ()

	////////////////////////////////////////////////////////////////////////////
	Address& Address::operator = (const Address& other)
	{
		memcpy (address_, other.getAddress (), sizeof (NLaddress));
		return *this;
	} // end Address ()

	////////////////////////////////////////////////////////////////////////////
	Address::operator const char* () const
	{
		return toString ().c_str ();
	} // end operator const char* ()


	////////////////////////////////////////////////////////////////////////////
	Address::operator bool () const
	{
		return isValid ();
	} // end operator bool ()
} // end namespace RaptorNL
