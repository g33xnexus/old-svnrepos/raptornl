/*
  HawkNL cross platform network library
  Copyright (C) 2000-2004 Phil Frisbie, Jr. (phil@hawksoft.com)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Library General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Library General Public License for more details.

  You should have received a copy of the GNU Library General Public
  License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
  Boston, MA  02111-1307, USA.

  Or go to http://www.gnu.org/copyleft/lgpl.html
*/

#ifndef SOCKETS_H
#define SOCKETS_H

namespace RaptorNL
{
	namespace __PRIVATE__
	{
		bool sock_Init(void);
		void sock_Shutdown(void);
		bool sock_Listen(NLsocket socket);
		NLsocket sock_AcceptConnection(NLsocket socket);
		NLsocket sock_Open(uint16_t port, SocketType type);
		bool sock_Connect(NLsocket socket, const NLaddress *address);
		void sock_Close(NLsocket socket);
		int32_t sock_Read(NLsocket socket, /*@out@*/ void *buffer, int32_t nbytes);
		int32_t sock_Write(NLsocket socket, const void *buffer, int32_t nbytes);
		NLchar *sock_AddrToString(const NLaddress *address, /*@returned@*/ /*@out@*/ NLchar *string);
		bool sock_StringToAddr(const NLchar *string, /*@out@*/ NLaddress *address);
		bool sock_GetLocalAddr(NLsocket socket, /*@out@*/ NLaddress *address);
		NLaddress *sock_GetAllLocalAddr(/*@out@*/ int32_t *count);
		bool sock_SetLocalAddr(const NLaddress *address);
		NLchar *sock_GetNameFromAddr(const NLaddress *address, /*@returned@*/ /*@out@*/ NLchar *name);
		bool sock_GetNameFromAddrAsync(const NLaddress *address, /*@out@*/ NLchar *name);
		bool sock_GetAddrFromName(const NLchar *name, /*@out@*/ NLaddress *address);
		bool sock_GetAddrFromNameAsync(const NLchar *name, /*@out@*/ NLaddress *address);
		bool sock_AddrCompare(const NLaddress *address1, const NLaddress *address2);
		uint16_t sock_GetPortFromAddr(const NLaddress *address);
		void sock_SetAddrPort(NLaddress *address, uint16_t port);
		Error sock_GetSystemError(void);
		int32_t sock_PollGroup(int32_t group, NLenum name, /*@out@*/ NLsocket *sockets, int32_t number, int32_t timeout);
		bool sock_PollSocket(NLsocket socket, NLenum name, int32_t timeout);
		bool sock_Hint(NLenum name, int32_t arg);
		bool sock_SetSocketOpt(NLsocket socket, NLenum name, int32_t arg);
		int32_t sock_GetSocketOpt(NLsocket socket, NLenum name);
	}
}

#endif /* SOCKETS_H */

