#include "raptornl.h"
#include "Socket.h"
#include "Buffer.h"

namespace RaptorNL
{
	////////////////////////////////////////////////////////////////////////////
	/* Construct a new Socket object.
	 */
	Socket::Socket ()
		:	socket_ (NL_INVALID)
	{
	} // end Socket ()


	////////////////////////////////////////////////////////////////////////////
	/* Construct a new Socket object.
	 */
	Socket::Socket (NLsocket sock)
		:	socket_ (sock)
	{
	} // end Socket ()


	////////////////////////////////////////////////////////////////////////////
	/* Dtor
	 */
	Socket::~Socket ()
	{
	} // end ~Socket ()


	////////////////////////////////////////////////////////////////////////////
	NLsocket Socket::getSocket () const
	{
		return socket_;
	} // end socket ()


	////////////////////////////////////////////////////////////////////////////
	/* Open this socket.
	 */
	bool Socket::open (uint16_t port, SocketType type)
	{
		socket_ = __PRIVATE__::nlOpen (port, type);
		if (socket_ == NL_INVALID)
		{
			return false;
		}
		return true;
	} // end open ()


	////////////////////////////////////////////////////////////////////////////
	/* Close this socket.
	 */
	bool Socket::close ()
	{
		if (__PRIVATE__::nlClose (socket_))
		{
			socket_ = NL_INVALID;
			return true;
		}
		return false;
	} // end close ()


	////////////////////////////////////////////////////////////////////////////
	/* Test if this socket is valid.
	 */
	bool Socket::isValid ()
	{
		return socket_ != NL_INVALID;
	} // end close ()


	////////////////////////////////////////////////////////////////////////////
	/* Read from this socket into the given buffer
	 */
	size_t Socket::read (void* buffer, size_t nbytes)
	{
		return __PRIVATE__::nlRead (socket_, buffer, nbytes);
	} // end read ()


	////////////////////////////////////////////////////////////////////////////
	Buffer* Socket::read ()
	{
		size_t buflen = 0;
		if (__PRIVATE__::nlRead (socket_, &buflen, 4) == 4)
		{
			int8_t tempBuffer[buflen + 1];
			size_t read = __PRIVATE__::nlRead (socket_, tempBuffer, buflen);
			tempBuffer[read + 1] = '\0';
			Buffer* buf = new Buffer (tempBuffer);
			return buf;
		}
		return NULL;
	} // end read ()


	////////////////////////////////////////////////////////////////////////////
	/* Write from the buffer to the socket
	 */
	size_t Socket::write (const void* buffer, size_t nbytes)
	{
		return __PRIVATE__::nlWrite (socket_, buffer, nbytes);
	} // end write ()


	////////////////////////////////////////////////////////////////////////////
	void Socket::write (const Buffer* buffer)
	{
		Buffer::Iterator it = buffer->getIterator ();
		size_t buflen = buffer->getSize ();
		__PRIVATE__::nlWrite (socket_, (void*) &buflen, 4);
		__PRIVATE__::nlWrite (socket_, it.getBlock (buflen), buflen);
	} // end write ()


	////////////////////////////////////////////////////////////////////////////
	/* Get the packets sent since the last clearPacketsSent ().
	 */
	int64_t Socket::packetsSent ()
	{
		return __PRIVATE__::nlGetSocketStat (socket_, PACKETS_SENT);
	} // end packetsSent ()


	////////////////////////////////////////////////////////////////////////////
	/* Clear the number of packets sent.
	 */
	bool Socket::clearPacketsSent ()
	{
		return __PRIVATE__::nlClearSocketStat (socket_, PACKETS_SENT);
	} // end clearPacketsSent ()


	////////////////////////////////////////////////////////////////////////////
	/* Get the number of bytes sent since the last clearBytesSent ().
	 */
	int64_t Socket::bytesSent ()
	{
		return __PRIVATE__::nlGetSocketStat (socket_, BYTES_SENT);
	} // end bytesSent ()


	////////////////////////////////////////////////////////////////////////////
	/* Clear the number of bytes sent.
	 */
	bool Socket::clearBytesSent ()
	{
		return __PRIVATE__::nlClearSocketStat (socket_, BYTES_SENT);
	} // end clearBytesSent ()


	////////////////////////////////////////////////////////////////////////////
	/* Get the average bytes sent for the last 8 seconds.
	 */
	int64_t Socket::averageBytesSent ()
	{
		return __PRIVATE__::nlGetSocketStat (socket_, AVE_BYTES_SENT);
	} // end averageBytesSent ()


	////////////////////////////////////////////////////////////////////////////
	/* Reset the average number of bytes sent.
	 */
	bool Socket::clearAverageBytesSent ()
	{
		return __PRIVATE__::nlClearSocketStat (socket_, AVE_BYTES_SENT);
	} // end clearAverageBytesSent ()


	////////////////////////////////////////////////////////////////////////////
	/* Get the highest number of bytes sent per second.
	 */
	int64_t Socket::highBytesSent ()
	{
		return __PRIVATE__::nlGetSocketStat (socket_, HIGH_BYTES_SENT);
	} // end highBytesSent ()


	////////////////////////////////////////////////////////////////////////////
	/* Clear the record of the highest number of bytes sent in one second.
	 */
	bool Socket::clearHighBytesSent ()
	{
		return __PRIVATE__::nlClearSocketStat (socket_, HIGH_BYTES_SENT);
	} // end clearHighBytesSent ()


	////////////////////////////////////////////////////////////////////////////
	/* Return the number of packets received since the last clearPacketsReceived ().
	 */
	int64_t Socket::packetsReceived ()
	{
		return __PRIVATE__::nlGetSocketStat (socket_, PACKETS_RECEIVED);
	} // end packetsReceived ()


	////////////////////////////////////////////////////////////////////////////
	/* Clear the number of packets received.
	 */
	bool Socket::clearPacketsReceived ()
	{
		return __PRIVATE__::nlClearSocketStat (socket_, PACKETS_RECEIVED);
	} // end clearPacketsReceived ()


	////////////////////////////////////////////////////////////////////////////
	/* Get the number of bytes received since the last clearBytesReceived ().
	 */
	int64_t Socket::bytesReceived ()
	{
		return __PRIVATE__::nlGetSocketStat (socket_, BYTES_RECEIVED);
	} // end bytesReceived ()


	////////////////////////////////////////////////////////////////////////////
	/* Clear the number of bytes received.
	 */
	bool Socket::clearBytesReceived ()
	{
		return __PRIVATE__::nlClearSocketStat (socket_, BYTES_RECEIVED);
	} // end clearPacketsReceived ()


	////////////////////////////////////////////////////////////////////////////
	/* Get the average bytes received for the last 8 seconds.
	 */
	int64_t Socket::averageBytesReceived ()
	{
		return __PRIVATE__::nlGetSocketStat (socket_, AVE_BYTES_RECEIVED);
	} // end averageBytesReceived ()


	////////////////////////////////////////////////////////////////////////////
	/* Reset the average number of bytes received.
	 */
	bool Socket::clearAverageBytesReceived ()
	{
		return __PRIVATE__::nlClearSocketStat (socket_, AVE_BYTES_RECEIVED);
	} // end clearAverageBytesReceived ()


	////////////////////////////////////////////////////////////////////////////
	/* Get the highest number of bytes received in one second.
	 */
	int64_t Socket::highestBytesReceived ()
	{
		return __PRIVATE__::nlGetSocketStat (socket_, HIGH_BYTES_RECEIVED);
	} // end highestBytesReceived ()


	////////////////////////////////////////////////////////////////////////////
	/* Clear the number highest number of bytes received in one second.
	 */
	bool Socket::clearHighestBytesReceived ()
	{
		return __PRIVATE__::nlClearSocketStat (socket_, HIGH_BYTES_RECEIVED);
	} // end clearHighestBytesReceived ()
} // end namespace RaptorNL
