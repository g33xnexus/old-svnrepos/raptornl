/*
  HawkNL cross platform network library
  Copyright (C) 2000-2004 Phil Frisbie, Jr. (phil@hawksoft.com)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Library General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Library General Public License for more details.

  You should have received a copy of the GNU Library General Public
  License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
  Boston, MA  02111-1307, USA.

  Or go to http://www.gnu.org/copyleft/lgpl.html
*/

#include <string.h>
#include "raptornl_internal.h"
#include "sock.h"
#include "serial.h"
#include "parallel.h"

#ifdef NL_INCLUDE_LOOPBACK
#include "loopback.h"
#endif

#if defined HL_WINDOWS_APP && defined NL_INCLUDE_IPX
  #include "ipx.h"
#endif

#define MAX_NET_DRIVERS	 6

namespace RaptorNL
{
	namespace __PRIVATE__
	{
		volatile nl_state_t nlState = {false, true};

		/* mutexes for global variables */
		static HTmutex  socklock, instatlock, outstatlock;

		static volatile bool nlBlocking = false;

		static volatile nl_stats_t nlInstats;
		static volatile nl_stats_t nlOutstats;

		static volatile NLsocket nlNextsocket = 0;
		static volatile int32_t nlNumsockets = 0;
		static volatile int32_t nlMaxNumsockets = NL_MIN_SOCKETS; /* this is dynamic, and can grow as needed */
		static volatile int32_t nlInitCount = 0;

		pnl_socket_t *nlSockets = NULL;

		/* the current selected driver */
		static nl_netdriver_t /*@null@*/*driver = NULL;

		static nl_netdriver_t netdrivers[] =
		{
			{
			(NLchar*)TEXT("NL_IP"),
			(NLchar*)TEXT("NL_RELIABLE NL_UNRELIABLE NL_RELIABLE_PACKETS NL_BROADCAST NL_UDP_MULTICAST NL_MULTICAST"),
			IP,
			false,
			sock_Init,
			sock_Shutdown,
			sock_Listen,
			sock_AcceptConnection,
			sock_Open,
			sock_Connect,
			sock_Close,
			sock_Read,
			sock_Write,
			sock_AddrToString,
			sock_StringToAddr,
			sock_GetLocalAddr,
			sock_GetAllLocalAddr,
			sock_SetLocalAddr,
			sock_GetNameFromAddr,
			sock_GetNameFromAddrAsync,
			sock_GetAddrFromName,
			sock_GetAddrFromNameAsync,
			sock_AddrCompare,
			sock_GetPortFromAddr,
			sock_SetAddrPort,
			sock_GetSystemError,
			sock_PollGroup,
			sock_PollSocket,
			sock_Hint,
			sock_SetSocketOpt,
			sock_GetSocketOpt
			}
		#ifdef NL_INCLUDE_LOOPBACK
			,
			{
			(NLchar*)TEXT("NL_LOOP_BACK"),
			(NLchar*)TEXT("NL_RELIABLE NL_UNRELIABLE NL_RELIABLE_PACKETS NL_BROADCAST"),
			NL_LOOP_BACK,
			false,
			loopback_Init,
			loopback_Shutdown,
			loopback_Listen,
			loopback_AcceptConnection,
			loopback_Open,
			loopback_Connect,
			loopback_Close,
			loopback_Read,
			loopback_Write,
			loopback_AddrToString,
			loopback_StringToAddr,
			loopback_GetLocalAddr,
			loopback_GetAllLocalAddr,
			loopback_SetLocalAddr,
			loopback_GetNameFromAddr,
			loopback_GetNameFromAddrAsync,
			loopback_GetAddrFromName,
			loopback_GetAddrFromNameAsync,
			loopback_AddrCompare,
			loopback_GetPortFromAddr,
			loopback_SetAddrPort,
			loopback_GetSystemError,
			loopback_PollGroup,
			loopback_PollSocket,
			loopback_Hint,
			loopback_SetSocketOpt,
			loopback_GetSocketOpt
			}
		#endif /* NL_INCLUDE_LOOPBACK */
		#if defined HL_WINDOWS_APP && defined NL_INCLUDE_IPX
			,
			{
			(NLchar*)TEXT("NL_IPX"),
			(NLchar*)TEXT("NL_RELIABLE NL_UNRELIABLE NL_RELIABLE_PACKETS NL_BROADCAST"),
			NL_IPX,
			false,
			ipx_Init,
			ipx_Shutdown,
			sock_Listen,
			ipx_AcceptConnection,
			ipx_Open,
			sock_Connect,
			sock_Close,
			sock_Read,
			sock_Write,
			ipx_AddrToString,
			ipx_StringToAddr,
			ipx_GetLocalAddr,
			ipx_GetAllLocalAddr,
			ipx_SetLocalAddr,
			ipx_GetNameFromAddr,
			ipx_GetNameFromAddrAsync,
			ipx_GetAddrFromName,
			ipx_GetAddrFromNameAsync,
			ipx_AddrCompare,
			ipx_GetPortFromAddr,
			ipx_SetAddrPort,
			sock_GetSystemError,
			sock_PollGroup,
			sock_PollSocket,
			ipx_Hint,
			ipx_SetSocketOpt,
			ipx_GetSocketOpt
			}
		#endif /* HL_WINDOWS_APP && NL_INCLUDE_IPX */
			,
			{
			(NLchar*)NULL,
			}
		};

		/*

		 Internal helper functions.

		*/

		static bool isSafeString(const NLchar *string)
		{
			int i;
			bool   nullfound = false;

			/* make sure string is null terminated at less than NL_MAX_STRING_LENGTH */
			for(i=0;i<NL_MAX_STRING_LENGTH;i++)
			{
			if(string[i] == (NLchar)'\0')
			{
				nullfound = true;
				break;
			}
			}
			if(nullfound == false)
			{
			return false;
			}
			/* check for formating characters */
			if(_tcsrchr(string, '%') != NULL)
			{
			return false;
			}
			return true;
		}

		static void safecat(NLchar *dest, const NLchar *src)
		{
			int len;

			if(isSafeString(dest) != true || isSafeString(src) != true)
			{
			/* don't do anything */
			return;
			}
			len = (int)_tcslen(dest);
			if( len < (NL_MAX_STRING_LENGTH - 1))
			{
			_tcsncat(dest, src, (size_t)(NL_MAX_STRING_LENGTH - len));
			dest[NL_MAX_STRING_LENGTH - 1] = (NLchar)'\0';
			}
		}

		NLsocket nlGetNewSocket(void)
		{
			NLsocket	newsocket = NL_INVALID;
			nl_socket_t *sock = NULL;

			if(htMutexLock(&socklock) != 0)
			{
			nlSetError(SYSTEM_ERROR);
			return NL_INVALID;
			}
			if(nlNumsockets == nlMaxNumsockets)
			{
			nl_socket_t **temp;
			int32_t	   tempmaxnumsockets = nlMaxNumsockets;

			/* expand the list of sockets pointers */
			tempmaxnumsockets *= 2;
			temp = (nl_socket_t **)realloc((void *)nlSockets, tempmaxnumsockets * sizeof(nl_socket_t *));
			if(temp == NULL)
			{
				(void)htMutexUnlock(&socklock);
				nlSetError(OUT_OF_MEMORY);
				return NL_INVALID;
			}
			nlSockets = temp;
			nlMaxNumsockets = tempmaxnumsockets;
			}
			/* get a socket number */
			if(nlNumsockets == (int32_t)nlNextsocket)
			{
			newsocket = nlNextsocket++;
			/* allocate the memory */
			sock = (nl_socket_t *)malloc(sizeof(nl_socket_t));
			if(sock == NULL)
			{
				(void)htMutexUnlock(&socklock);
				nlSetError(OUT_OF_MEMORY);
				return NL_INVALID;
			}
			else
			{
				nlSockets[newsocket] = sock;
			}
			/* clear the structure */
			memset(sock, 0, sizeof(nl_socket_t));

			if(htMutexInit(&sock->readlock) != 0 || htMutexInit(&sock->writelock) != 0)
			{
				nlSetError(SYSTEM_ERROR);
				(void)htMutexUnlock(&socklock);
				return NL_INVALID;
			}
			}
			else
			/* there is an open socket slot somewhere below nlNextsocket */
			{
			NLsocket	i;
			HTmutex	 readlock, writelock;

			for(i=0;i<nlNextsocket;i++)
			{
				if(nlSockets[i]->inuse == false)
				{
				/* found an open socket slot */
				newsocket = i;
				sock = nlSockets[i];
				break;
				}
			}
			/* let's check just to make sure we did find a socket */
			if(sock == NULL)
			{
				(void)htMutexUnlock(&socklock);
				nlSetError(OUT_OF_MEMORY);
				return NL_INVALID;
			}
			readlock = sock->readlock;
			writelock = sock->writelock;
			/* clear the structure */
			memset(sock, 0, sizeof(nl_socket_t));
			sock->readlock = readlock;
			sock->writelock = writelock;
			}

			/* sockets are blocking until set for non-blocking */
			sock->blocking = nlBlocking;
			sock->inuse = true;
			nlNumsockets++;
			(void)htMutexUnlock(&socklock);
			return newsocket;
		}

		static void nlReturnSocket(NLsocket socket)
		{
			nl_socket_t	 *sock = nlSockets[socket];

			if((sock != NULL) && (sock->inuse == true))
			{
			sock->inuse = false;
			if(sock->inbuf != NULL)
			{
				free(sock->inbuf);
				sock->inbuf = NULL;
			}
			if(sock->outbuf != NULL)
			{
				free(sock->outbuf);
				sock->outbuf = NULL;
			}
			nlNumsockets--;
			}
		}

		void nlFreeSocket(NLsocket socket)
		{
			nl_socket_t	 *sock = nlSockets[socket];

			if(sock != NULL)
			{
			if(sock->inbuf != NULL)
			{
				free(sock->inbuf);
			}
			if(sock->outbuf != NULL)
			{
				free(sock->outbuf);
			}
			(void)htMutexDestroy(&sock->readlock);
			(void)htMutexDestroy(&sock->writelock);
			free(sock);
			}
		}

		bool nlIsValidSocket(NLsocket socket)
		{
			nl_socket_t *sock;

			if(socket < 0 || socket > nlMaxNumsockets)
			{
			nlSetError(INVALID_SOCKET);
			return false;
			}
			sock = nlSockets[socket];
			if(sock == NULL)
			{
			nlSetError(INVALID_SOCKET);
			return false;
			}
			if(sock->inuse == false)
			{
			nlSetError(INVALID_SOCKET);
			return false;
			}
			return true;
		}

		bool nlLockSocket(NLsocket socket, int32_t which)
		{
			nl_socket_t	 *sock = nlSockets[socket];

			if((which&NL_READ) > 0)
			{
			if(htMutexLock(&sock->readlock) != 0)
			{
				nlSetError(SYSTEM_ERROR);
				return false;
			}
			}
			if((which&NL_WRITE) > 0)
			{
			if(htMutexLock(&sock->writelock) != 0)
			{
				if((which&NL_READ) > 0)
				{
				(void)htMutexUnlock(&sock->readlock);
				}
				nlSetError(SYSTEM_ERROR);
				return false;
			}
			}
			return true;
		}

		void nlUnlockSocket(NLsocket socket, int32_t which)
		{
			nl_socket_t	 *sock = nlSockets[socket];

			if((which&NL_WRITE) > 0)
			{
			(void)htMutexUnlock(&sock->writelock);
			}
			if((which&NL_READ) > 0)
			{
			(void)htMutexUnlock(&sock->readlock);
			}
		}

		static void nlUpdateStats(volatile nl_stats_t *stats, int32_t nbytes, int32_t npackets)
		{
			time_t  t;

			(void)time(&t);
			if(stats->stime == 0)
			{
			/* must be the first time through */
			stats->stime = t;
			stats->lastbucket = -1;
			stats->firstround = true;
			}
			/* do the basic update */
			stats->packets += npackets;
			stats->bytes += nbytes;

			/* check to see if we need to do the full update */
			if(stats->stime != t)
			{
			int32_t   i;
			int32_t  count = 0;
			time_t  diff = t - stats->stime;

			stats->stime = t;

			if(stats->curbytes > stats->highest)
			{
				stats->highest = stats->curbytes;
			}
			if(diff >= NL_NUM_BUCKETS)
			{
				diff = NL_NUM_BUCKETS;
			}

			while(diff > 1)
			{
				/* we need to zero out skipped over buckets */
				stats->lastbucket++;
				if(stats->lastbucket == NL_NUM_BUCKETS)
				{
				stats->lastbucket = 0;
				}
				stats->bucket[stats->lastbucket] = 0;
				diff--;
			}
			stats->lastbucket++;
			if(stats->lastbucket == NL_NUM_BUCKETS)
			{
				stats->lastbucket = 0;
				stats->firstround = false;
			}
			stats->bucket[stats->lastbucket] = stats->curbytes;
			if(stats->firstround == true)
			{
				/* this corrects the stats for the first second */
				for(i=stats->lastbucket + 1;i<NL_NUM_BUCKETS;i++)
				{
				stats->bucket[i] = stats->curbytes;
				}
			}
			stats->curbytes = 0;

			for(i=0;i<NL_NUM_BUCKETS;i++)
			{
				count += stats->bucket[i];
			}
			stats->average =  count / NL_NUM_BUCKETS;
			}
			stats->curbytes += nbytes;
		}

		static void nlUpdateInStats(int32_t nbytes, int32_t npackets)
		{
			if(nlState.socketStats == false)
			{
			return;
			}
			(void)htMutexLock(&instatlock);
			nlUpdateStats(&nlInstats, nbytes, npackets);
			(void)htMutexUnlock(&instatlock);
		}

		static void nlUpdateOutStats(int32_t nbytes, int32_t npackets)
		{
			if(nlState.socketStats == false)
			{
			return;
			}
			(void)htMutexLock(&outstatlock);
			nlUpdateStats(&nlOutstats, nbytes, npackets);
			(void)htMutexUnlock(&outstatlock);
		}

		static void nlUpdateSocketInStats(NLsocket socket, int32_t nbytes, int32_t npackets)
		{
			nl_socket_t	 *sock = nlSockets[socket];

			if(nlState.socketStats == false)
			{
			return;
			}
			nlUpdateStats(&sock->instats, nbytes, npackets);
		}

		static void nlUpdateSocketOutStats(NLsocket socket, int32_t nbytes, int32_t npackets)
		{
			nl_socket_t	 *sock = nlSockets[socket];

			if(nlState.socketStats == false)
			{
			return;
			}
			nlUpdateStats(&sock->outstats, nbytes, npackets);
		}

		/*

		  Low level functions, a thin layer over Sockets.

		*/

		/*
		   Trys to init all drivers, BUT DOES NOT SELECT A DRIVER
		*/

		HL_EXP bool HL_APIENTRY nlInit(void)
		{
			int i, numdrivers = 0;

			nlSetError(NO_ERROR);
			/* init socket memory, mutexes, and global variables */
			if(nlInitCount == 0)
			{
			nlMaxNumsockets = NL_MIN_SOCKETS;
			if(nlSockets == NULL)
			{
				nlSockets = (nl_socket_t **)malloc(nlMaxNumsockets * sizeof(nl_socket_t *));
			}
			if(nlSockets == NULL)
			{
				nlSetError(OUT_OF_MEMORY);
				nlShutdown();
				return false;
			}
			if(nlGroupInit() == false)
			{
				nlShutdown();
				return false;
			}
			if(htMutexInit(&socklock) != 0 || htMutexInit(&instatlock) != 0 ||
				htMutexInit(&outstatlock) != 0)
			{
				nlSetError(SYSTEM_ERROR);
				nlShutdown();
				return false;
			}
			nlNumsockets = 0;
			nlNextsocket = 0;
			nlBlocking = false;
			nlState.socketStats = false;
			nlState.nl_big_endian_data = true;

			for(i=0;i<MAX_NET_DRIVERS;i++)
			{
				if(netdrivers[i].name == NULL)
				{
				break;
				}
				if(netdrivers[i].initialized == true)
				{
				numdrivers++;
				}
				else if(netdrivers[i].Init() == true)
				{
				netdrivers[i].initialized = true;
				numdrivers++;
				}
			}
			if(numdrivers == 0)
			{
				nlSetError(NO_NETWORK);
				nlShutdown();
				return false;
			}
			}
			nlInitCount++;
			return true;
		}

		/*
		   Called at the end of your program, shuts down the active driver and frees memory
		*/

		HL_EXP void HL_APIENTRY nlShutdown(void)
		{
			--nlInitCount;
			
			if(nlInitCount > 0)
			{
			return;
			}
			if(driver != NULL)
			{
			/* close any open sockets */
			(void)htMutexLock(&socklock);
			if(nlSockets != NULL)
			{
				NLsocket s;
				
				for(s=0;s<nlNextsocket;s++)
				{
				if(nlSockets[s] != NULL)
				{
					if(nlIsValidSocket(s) == true)
					{
					driver->Close(s);
					htThreadYield();
					}
				}
				}
			}
			/* now we can shutdown the driver */
			driver->Shutdown();
			driver->initialized = false;
			driver = NULL;
			}
			else
			{
			nlSetError(NO_NETWORK);
			}
			
			htThreadSleep(1);
			
			/* now free all the socket structures */
			if(nlSockets != NULL)
			{
			NLsocket s;
			
			for(s=0;s<nlNextsocket;s++)
			{
				if(nlSockets[s] != NULL)
				{
				if(nlIsValidSocket(s) == true)
				{
					(void)nlLockSocket(s, NL_BOTH);
					nlReturnSocket(s);
					nlUnlockSocket(s, NL_BOTH);
					htThreadYield();
				}
				nlFreeSocket(s);
				}
			}
			free(nlSockets);
			nlSockets = NULL;
			}
			(void)htMutexUnlock(&socklock);
			nlGroupShutdown();
			/* destroy the mutexes */
			(void)htMutexDestroy(&socklock);
			(void)htMutexDestroy(&instatlock);
			(void)htMutexDestroy(&outstatlock);
		}

		/*
		   Enables a socket to listen for incomming connections
		*/

		HL_EXP bool HL_APIENTRY nlListen(NLsocket socket)
		{
			if(driver != NULL)
			{
			if(nlIsValidSocket(socket) == true)
			{
				bool result;

				if(nlLockSocket(socket, NL_BOTH) == false)
				{
				return false;
				}
				result = driver->Listen(socket);
				nlUnlockSocket(socket, NL_BOTH);
				return result;
			}
			nlSetError(INVALID_SOCKET);
			return false;
			}
			nlSetError(NO_NETWORK);
			return false;
		}

		/*
		   Accepts a pending connection.
		   Creates a new socket object for this connection.
		*/

		HL_EXP NLsocket HL_APIENTRY nlAcceptConnection(NLsocket socket)
		{
			if(driver)
			{
			if(nlIsValidSocket(socket) == true)
			{
				NLsocket newsocket;

				if(nlLockSocket(socket, NL_BOTH) == false)
				{
				return NL_INVALID;
				}
				newsocket = driver->AcceptConnection(socket);
				nlUnlockSocket(socket, NL_BOTH);
				if(newsocket != NL_INVALID)
				{
				/* the new socket was locked when it is created */
				nlUnlockSocket(newsocket, NL_BOTH);
				}
				return newsocket;
			}
			nlSetError(INVALID_SOCKET);
			return NL_INVALID;
			}
			nlSetError(NO_NETWORK);
			return NL_INVALID;
		}

		/*
		   Creates a new socket object.
		   Can be used for reading or broadcast as is.
		   For non-broadcast use, call nlConnectSocket to connect to a remote address.
		*/

		HL_EXP NLsocket HL_APIENTRY nlOpen(uint16_t port, SocketType type)
		{
			if(driver)
			{
				return (driver->Open(port, type));
			}

			nlSetError(NO_NETWORK);
			return NL_INVALID;
		}

		/*
		   Connect a socket to a remote address.
		*/

		HL_EXP bool HL_APIENTRY nlConnect(NLsocket socket, const NLaddress *address)
		{
			if(driver)
			{
			if(nlIsValidSocket(socket) == true)
			{
				if(address == NULL)
				{
				nlSetError(NULL_POINTER);
				}
				else
				{
				bool result;

				if(nlLockSocket(socket, NL_BOTH) == false)
				{
					return false;
				}
				result = driver->Connect(socket, address);
				nlUnlockSocket(socket, NL_BOTH);
				return result;
				}
			}
			else
			{
				nlSetError(INVALID_SOCKET);
			}
			return false;
			}
			nlSetError(NO_NETWORK);
			return false;
		}

		/*
		   Close the socket.
		*/

		HL_EXP bool HL_APIENTRY nlClose(NLsocket socket)
		{
			if(driver)
			{
			if(nlIsValidSocket(socket) == true)
			{
				if(htMutexLock(&socklock) != 0)
				{
				nlSetError(SYSTEM_ERROR);
				return false;
				}
				if(nlLockSocket(socket, NL_BOTH) == false)
				{
				return false;
				}
				driver->Close(socket);
				/* return the socket for reuse */
				nlReturnSocket(socket);
				nlUnlockSocket(socket, NL_BOTH);
				if(htMutexUnlock(&socklock) != 0)
				{
				nlSetError(SYSTEM_ERROR);
				return false;
				}
				return true;
			}
			else
			{
				nlSetError(INVALID_SOCKET);
				return true;
			}
			}
			nlSetError(NO_NETWORK);
			return false;
		}

		/*
		   Reads from a socket.
		*/

		HL_EXP int32_t HL_APIENTRY nlRead(NLsocket socket, void *buffer, int32_t nbytes)
		{
			if(driver)
			{
			if(nlIsValidSocket(socket) == true)
			{
				if(buffer == NULL)
				{
				nlSetError(NULL_POINTER);
				}
				else
				{
				int32_t received;

				if(nlLockSocket(socket, NL_READ) == false)
				{
					return NL_INVALID;
				}
				received = driver->Read(socket, buffer, nbytes);

				if(received > 0)
				{
					nlUpdateSocketInStats(socket, received, 1);
					nlUpdateInStats(received, 1);
				}
				nlUnlockSocket(socket, NL_READ);
				return received;
				}
			}
			else
			{
				nlSetError(INVALID_SOCKET);
			}
			return NL_INVALID;
			}
			nlSetError(NO_NETWORK);
			return NL_INVALID;
		}

		/*
		   Writes to a socket.
		*/

		HL_EXP int32_t HL_APIENTRY nlWrite(NLsocket socket, const void *buffer, int32_t nbytes)
		{
			if(driver)
			{
			/* check for group */
			if(socket >= NL_FIRST_GROUP)
			{
				int32_t	   number = NL_MAX_GROUP_SOCKETS;
				NLsocket	s[NL_MAX_GROUP_SOCKETS];
				int32_t	   i;
				int32_t	   sent = nbytes;

				if(nlGroupGetSockets((int32_t)socket, (NLsocket *)s, &number) == false)
				{
				return NL_INVALID;
				}

				for(i=0;i<number;i++)
				{
				int32_t result;

				if(nlIsValidSocket(s[i]) == true)
				{
					result = nlWrite(s[i], buffer, nbytes);
					if(result < sent)
					{
					sent = result;
					}
				}
				}
				return sent;
			}
			else
			{
				if(nlIsValidSocket(socket) == true)
				{
				if(buffer == NULL)
				{
					nlSetError(NULL_POINTER);
				}
				else
				{
					int32_t sent;

					if(nlLockSocket(socket, NL_WRITE) == false)
					{
					return NL_INVALID;
					}
					sent = driver->Write(socket, buffer, nbytes);
					if(sent > 0)
					{
					nlUpdateSocketOutStats(socket, sent, 1);
					nlUpdateOutStats(sent, 1);
					}
					nlUnlockSocket(socket, NL_WRITE);
					return sent;
				}
				}
				else
				{
				nlSetError(INVALID_SOCKET);
				}
				return NL_INVALID;
			}
			}
			nlSetError(NO_NETWORK);
			return NL_INVALID;
		}

		/*
		   Polls all sockets in the group to see which have data waiting to be read.
		   nlPollGroup uses select() on TCP or UDP sockets.
		   Returns number of sockets waiting, and a list of those sockets, or NL_INVALID
		   on an error.
		*/

		HL_EXP int32_t HL_APIENTRY nlPollGroup(int32_t group, NLenum name, /*@out@*/ NLsocket *sockets, int32_t number, int32_t timeout)
		{
			if(driver)
			{
			if(sockets == NULL )
			{
				nlSetError(NULL_POINTER);
				return 0;
			}
			return (driver->PollGroup(group, name, sockets, number, timeout));
			}
			nlSetError(NO_NETWORK);
			return 0;
		}

		HL_EXP bool HL_APIENTRY nlPollSocket(NLsocket socket, NLenum name, int32_t timeout)
		{
			if(driver)
			{
			if(nlIsValidSocket(socket) == true )
			{
				return (driver->PollSocket(socket, name, timeout));
			}
			else
			{
				nlSetError(INVALID_SOCKET);
				return false;
			}
			}
			nlSetError(NO_NETWORK);
			return false;
		}

		HL_EXP bool HL_APIENTRY nlHint(NLenum name, int32_t arg)
		{
			if(driver)
			{
			return driver->Hint(name, arg);
			}
			nlSetError(NO_NETWORK);
			return false;
		}

		HL_EXP bool HL_APIENTRY nlSetSocketOpt(NLsocket socket, NLenum name, int32_t arg)
		{
			if(driver)
			{
			if(nlIsValidSocket(socket) == true )
			{
				return driver->SetSocketOpt(socket, name, arg);
			}
			else
			{
				nlSetError(INVALID_SOCKET);
				return false;
			}
			}
			nlSetError(NO_NETWORK);
			return false;
		}

		HL_EXP int32_t HL_APIENTRY nlGetSocketOpt(NLsocket socket, NLenum name)
		{
			if(driver)
			{
			if(nlIsValidSocket(socket) == true )
			{
				return driver->GetSocketOpt(socket, name);
			}
			else
			{
				nlSetError(INVALID_SOCKET);
				return NL_INVALID;
			}
			}
			nlSetError(NO_NETWORK);
			return NL_INVALID;
		}

		/*
		   Converts the numeric address in the NLaddress structure to a string.
		*/

		HL_EXP /*@null@*/ NLchar* HL_APIENTRY nlAddrToString(const NLaddress *address, NLchar *string)
		{
			if(driver)
			{
			if((string == NULL) || (address == NULL))
			{
				nlSetError(NULL_POINTER);
				return NULL;
			}
			return (driver->AddrToString(address, string));
			}
			nlSetError(NO_NETWORK);
			return NULL;
		}

		/*
		   Takes a string that contains a full network address (ie, for IP 192.168.0.1:27000),
		   and adds it to the NLaddress structure.
		*/

		HL_EXP bool HL_APIENTRY nlStringToAddr(const NLchar *string, NLaddress *address)
		{
			if(driver)
			{
			if((string == NULL) || (address == NULL))
			{
				nlSetError(NULL_POINTER);
				return false;
			}
			if(isSafeString(string) == false)
			{
				nlSetError(STRING_OVER_RUN);
				return false;
			}
			return driver->StringToAddr(string, address);
			}
			nlSetError(NO_NETWORK);
			return false;
		}

		/*
		   Gets the remote address of the socket if connected to a remote host,
		   or the local address if not connected.
		*/

		/* Note: the drivers put a copy of address in nl_socket_t, so we just need to copy it */
		HL_EXP bool HL_APIENTRY nlGetRemoteAddr(NLsocket socket, NLaddress *address)
		{
			if(driver)
			{
			if(address == NULL)
			{
				nlSetError(NULL_POINTER);
				return false;
			}
			if(nlIsValidSocket(socket) == true)
			{
				nl_socket_t *sock = nlSockets[socket];

				if(nlLockSocket(socket, NL_READ) == false)
				{
				return false;
				}
				memcpy(address, &sock->addressin, sizeof(NLaddress));
					address->valid = true;
				nlUnlockSocket(socket, NL_READ);
			}
			else
			{
				nlSetError(INVALID_SOCKET);
				memset(address, 0, sizeof(NLaddress));
				return false;
			}
			return true;
			}
			nlSetError(NO_NETWORK);
			return false;
		}

		/*
		   Sets the remote address of an unconnected UDP socket.
		*/

		HL_EXP bool HL_APIENTRY nlSetRemoteAddr(NLsocket socket, const NLaddress *address)
		{
			if(driver)
			{
			if(nlIsValidSocket(socket) == true)
			{
				if(address == NULL)
				{
				nlSetError(NULL_POINTER);
				return false;
				}
				else
				{
				nl_socket_t *sock = nlSockets[socket];

				if(nlLockSocket(socket, NL_WRITE) == false)
				{
					return false;
				}
				memcpy(&sock->addressout, address, sizeof(NLaddress));
				nlUnlockSocket(socket, NL_WRITE);
				}
			}
			else
			{
				nlSetError(INVALID_SOCKET);
				return false;
			}
			return true;
			}
			nlSetError(NO_NETWORK);
			return false;
		}

		/*
		   Gets the local address.
		*/

		HL_EXP bool HL_APIENTRY nlGetLocalAddr(NLsocket socket, NLaddress *address)
		{
			if(driver)
			{
			if(nlIsValidSocket(socket) == true)
			{
				if(address == NULL)
				{
				nlSetError(NULL_POINTER);
				return false;
				}
				if(nlLockSocket(socket, NL_READ) == false)
				{
				return false;
				}
				if(driver->GetLocalAddr(socket, address) != true)
				{
				nlUnlockSocket(socket, NL_READ);
				return false;
				}
				nlUnlockSocket(socket, NL_READ);
			}
			else
			{
				nlSetError(INVALID_SOCKET);
				return false;
			}
			return true;
			}
			nlSetError(NO_NETWORK);
			return false;
		}

		HL_EXP NLaddress * HL_APIENTRY nlGetAllLocalAddr(/*@out@*/ int32_t *count)
		{
			if(driver)
			{
			if(count == NULL)
			{
				nlSetError(NULL_POINTER);
				return NULL;
			}
			return driver->GetAllLocalAddr(count);
			}
			nlSetError(NO_NETWORK);
			return NULL;
		}

		HL_EXP bool HL_APIENTRY nlSetLocalAddr(const NLaddress *address)
		{
			if(driver)
			{
			if(address == NULL)
			{
				nlSetError(NULL_POINTER);
				return false;
			}
			return driver->SetLocalAddr(address);
			}
			nlSetError(NO_NETWORK);
			return false;
		}

		/*
		   Resolves the name from the address.
		*/

		HL_EXP /*@null@*/ NLchar* HL_APIENTRY nlGetNameFromAddr(const NLaddress *address, NLchar *name)
		{
			if(driver)
			{
			if((name == NULL) || (address == NULL))
			{
				nlSetError(NULL_POINTER);
				return NULL;
			}
			return (driver->GetNameFromAddr(address, name));
			}
			nlSetError(NO_NETWORK);
			return NULL;
		}

		/*
		   Resolves the name from the address asynchronously.
		*/

		HL_EXP bool HL_APIENTRY nlGetNameFromAddrAsync(const NLaddress *address, NLchar *name)
		{
			if(driver)
			{
			if((name == NULL) || (address == NULL))
			{
				nlSetError(NULL_POINTER);
				return false;
			}
			return driver->GetNameFromAddrAsync(address, name);
			}
			nlSetError(NO_NETWORK);
			return false;
		}

		/*
		   Get the address from a host name.
		*/

		HL_EXP bool HL_APIENTRY nlGetAddrFromName(const NLchar *name, NLaddress *address)
		{
			if(driver)
			{
			if((name == NULL) || (address == NULL))
			{
				nlSetError(NULL_POINTER);
				return false;
			}
			if(isSafeString(name) == false)
			{
				nlSetError(STRING_OVER_RUN);
				return false;
			}
			return driver->GetAddrFromName(name, address);
			}
			nlSetError(NO_NETWORK);
			return false;
		}

		/*
		   Get the address from a host name asynchronously.
		*/

		HL_EXP bool HL_APIENTRY nlGetAddrFromNameAsync(const NLchar *name, NLaddress *address)
		{
			if(driver)
			{
			if((name == NULL) || (address == NULL))
			{
				nlSetError(NULL_POINTER);
				return false;
			}
			if(isSafeString(name) == false)
			{
				nlSetError(STRING_OVER_RUN);
				return false;
			}
			return driver->GetAddrFromNameAsync(name, address);
			}
			nlSetError(NO_NETWORK);
			return false;
		}

		/*
		   Compare two addresses.
		*/

		HL_EXP bool HL_APIENTRY nlAddrCompare(const NLaddress *address1, const NLaddress *address2)
		{
			if(driver)
			{
			if((address1 == NULL) || (address2 == NULL))
			{
				nlSetError(NULL_POINTER);
				return false;
			}
			return driver->AddrCompare(address1, address2);
			}
			nlSetError(NO_NETWORK);
			return false;
		}

		/*
		   Get the port number from an address.
		*/

		HL_EXP uint16_t HL_APIENTRY nlGetPortFromAddr(const NLaddress *address)
		{
			if(driver)
			{
			if(address == NULL)
			{
				nlSetError(NULL_POINTER);
				return 0;
			}
			return driver->GetPortFromAddr(address);
			}
			nlSetError(NO_NETWORK);
			return 0;
		}

		/*
		   Set the port number in the address.
		*/

		HL_EXP bool HL_APIENTRY nlSetAddrPort(NLaddress *address, uint16_t port)
		{
			if(driver)
			{
			if(address == NULL)
			{
				nlSetError(NULL_POINTER);
				return false;
			}
			driver->SetAddrPort(address, port);
			return true;
			}
			nlSetError(NO_NETWORK);
			return false;
		}


		/*

		  Non-socket functions

		*/

		/*
		   Select the network to use.
		*/

		HL_EXP bool HL_APIENTRY nlSelectNetwork(NetworkType network)
		{
			int i, found = 0;

			if(driver != NULL)
			{
			/* we cannot select a new network without shutting down first */
			nlSetError(SELECT_NET_ERROR);
			return false;
			}

			for(i=0;i<MAX_NET_DRIVERS;i++)
			{
			if(netdrivers[i].name == NULL)
			{
				break;
			}
			if(netdrivers[i].type == network)
			{
				found++;
				if(netdrivers[i].initialized == true)
				{
				driver = &netdrivers[i];
				return true;
				}
			}
			}
			if(found > 0)
			{
			nlSetError(INVALID_TYPE);
			}
			else
			{
			nlSetError(INVALID_ENUM);
			}
			return false;
		}

		/*
		   Returns a string corresponding to the NLenum.
		*/

		HL_EXP const /*@observer@*//*@null@*/ NLchar* HL_APIENTRY nlGetString(NLenum name)
		{
			/* use seperate strings for thread safety */
			static NLchar   vstring[NL_MAX_STRING_LENGTH];
			static NLchar   tstring[NL_MAX_STRING_LENGTH];
			int32_t i;

			nlSetError(NO_ERROR);
			/* intitialize the version string */
			_tcsncpy(vstring, (NLchar *)TEXT(RNL_VERSION_STRING), (size_t)NL_MAX_STRING_LENGTH);
			vstring[NL_MAX_STRING_LENGTH - 1] = (NLchar) '\0';
		#ifdef _UNICODE
			/* add the UNICODE string */
			safecat(vstring, (NLchar *)TEXT(" UNICODE version"));
		#endif
			/* intitialize the network types string */
			memset(tstring, 0, sizeof(NLchar) * NL_MAX_STRING_LENGTH);
			for(i=0;i<MAX_NET_DRIVERS;i++)
			{
			if(netdrivers[i].name == NULL)
			{
				break;
			}
			if(netdrivers[i].initialized == true)
			{
				safecat((NLchar *)tstring, (const NLchar *)netdrivers[i].name);
				safecat((NLchar *)tstring, (NLchar *)TEXT(" "));
			}
			}

			switch (name) {

			case VERSION:
				return (const NLchar*)vstring;

			case NETWORK_TYPES:
				return (const NLchar*)tstring;

			case SOCKET_TYPES:
			if(driver != NULL)
			{
					return (const NLchar*)(driver->socktypes);
			}
			else
			{
				nlSetError(NO_NETWORK);
			}
			break;

			default:
			nlSetError(INVALID_ENUM);
			}

			return NULL;
		}

		/*
		   Returns an integer corresponding to the NLenum.
		*/

		HL_EXP int32_t HL_APIENTRY nlGetInteger(NLenum name)
		{
			switch (name) {

			case PACKETS_SENT:
			return nlOutstats.packets;

			case BYTES_SENT:
			return nlOutstats.bytes;

			case AVE_BYTES_SENT:
			nlUpdateOutStats(0, 0);
			return nlOutstats.average;

			case HIGH_BYTES_SENT:
			return nlOutstats.highest;

			case PACKETS_RECEIVED:
			return nlInstats.packets;

			case BYTES_RECEIVED:
			return nlInstats.bytes;

			case AVE_BYTES_RECEIVED:
			nlUpdateInStats(0, 0);
			return nlInstats.average;

			case HIGH_BYTES_RECEIVED:
			return nlInstats.highest;

			case OPEN_SOCKETS:
			return nlNumsockets;

			default:
			nlSetError(INVALID_ENUM);
			}
			return 0;
		}

		/*
		   Clears the stat corresponding to the NLenum.
		*/

		HL_EXP bool HL_APIENTRY nlClear(Statistics name)
		{
			switch (name) {

			case PACKETS_SENT:
			if(htMutexLock(&outstatlock) != 0)
			{
				nlSetError(SYSTEM_ERROR);
				return false;
			}
			nlOutstats.packets = 0;
			(void)htMutexUnlock(&outstatlock);
			break;

			case BYTES_SENT:
			if(htMutexLock(&outstatlock) != 0)
			{
				nlSetError(SYSTEM_ERROR);
				return false;
			}
			nlOutstats.bytes = 0;
			(void)htMutexUnlock(&outstatlock);
			break;

			case AVE_BYTES_SENT:
			if(htMutexLock(&outstatlock) != 0)
			{
				nlSetError(SYSTEM_ERROR);
				return false;
			}
			nlOutstats.average = 0;
			memset((int8_t *)nlOutstats.bucket, 0, sizeof(int32_t) * NL_NUM_BUCKETS);
			(void)htMutexUnlock(&outstatlock);
			break;

			case HIGH_BYTES_SENT:
			if(htMutexLock(&outstatlock) != 0)
			{
				nlSetError(SYSTEM_ERROR);
				return false;
			}
			nlOutstats.highest = 0;
			(void)htMutexUnlock(&outstatlock);
			break;

			case PACKETS_RECEIVED:
			if(htMutexLock(&instatlock) != 0)
			{
				nlSetError(SYSTEM_ERROR);
				return false;
			}
			nlInstats.packets = 0;
			(void)htMutexUnlock(&instatlock);
			break;

			case BYTES_RECEIVED:
			if(htMutexLock(&instatlock) != 0)
			{
				nlSetError(SYSTEM_ERROR);
				return false;
			}
			nlInstats.bytes = 0;
			(void)htMutexUnlock(&instatlock);
			break;

			case AVE_BYTES_RECEIVED:
			if(htMutexLock(&instatlock) != 0)
			{
				nlSetError(SYSTEM_ERROR);
				return false;
			}
			nlInstats.average = 0;
			memset((int8_t *)nlInstats.bucket, 0, sizeof(int32_t) * NL_NUM_BUCKETS);
			(void)htMutexUnlock(&instatlock);
			break;

			case HIGH_BYTES_RECEIVED:
			if(htMutexLock(&instatlock) != 0)
			{
				nlSetError(SYSTEM_ERROR);
				return false;
			}
			nlInstats.highest = 0;
			(void)htMutexUnlock(&instatlock);
			break;

			case ALL_STATS:
			(void)nlClear(PACKETS_SENT);
			(void)nlClear(BYTES_SENT);
			(void)nlClear(AVE_BYTES_SENT);
			(void)nlClear(HIGH_BYTES_SENT);
			(void)nlClear(PACKETS_RECEIVED);
			(void)nlClear(BYTES_RECEIVED);
			(void)nlClear(AVE_BYTES_RECEIVED);
			(void)nlClear(HIGH_BYTES_RECEIVED);
			break;

			default:
			nlSetError(INVALID_ENUM);
			return false;
			}
			return true;
		}

		/*
		   Get the socket or system error.
		*/

		HL_EXP Error HL_APIENTRY nlGetSystemError(void)
		{
			if(driver)
			{
			return driver->GetSystemError();
			}
			return NO_NETWORK;
		}

		HL_EXP bool HL_APIENTRY nlEnable(NLenum name)
		{
			switch (name) {

			case NL_BLOCKING_IO:
			nlBlocking = true;
			break;

			case NL_TCP_NO_DELAY:
			return nlHint(NL_TCP_NO_DELAY, (int32_t)true);

			case NL_SOCKET_STATS:
			nlState.socketStats = true;
			break;

			case NL_BIG_ENDIAN_DATA:
			nlState.nl_big_endian_data = true;
			break;

			case NL_LITTLE_ENDIAN_DATA:
			nlState.nl_big_endian_data = false;
			break;

			default:
			nlSetError(INVALID_ENUM);
			return false;
			}
			return true;
		}

		HL_EXP bool HL_APIENTRY nlDisable(NLenum name)
		{
			switch (name) {

			case NL_BLOCKING_IO:
			nlBlocking = false;
			break;

			case NL_TCP_NO_DELAY:
			return nlHint(NL_TCP_NO_DELAY, false);

			case NL_SOCKET_STATS:
			nlState.socketStats = false;
			break;

			case NL_BIG_ENDIAN_DATA:
			nlState.nl_big_endian_data = false;
			break;

			case NL_LITTLE_ENDIAN_DATA:
			nlState.nl_big_endian_data = true;
			break;

			default:
			nlSetError(INVALID_ENUM);
			return false;
			}
			return true;
		}

		HL_EXP bool HL_APIENTRY nlGetBoolean(NLenum name)
		{
			switch (name) {

			case NL_BLOCKING_IO:
			return nlBlocking;

			case NL_SOCKET_STATS:
			return nlState.socketStats;

			case NL_BIG_ENDIAN_DATA:
			return nlState.nl_big_endian_data;

			case NL_LITTLE_ENDIAN_DATA:
			return (bool)(nlState.nl_big_endian_data == true ? false:true);

			default:
			nlSetError(INVALID_ENUM);
			return false;
			}
		}

		HL_EXP int32_t HL_APIENTRY nlGetSocketStat(NLsocket socket, NLenum name)
		{
			nl_socket_t	 *sock;
			int32_t		  result = 0;

			if(nlIsValidSocket(socket) == false)
			{
			nlSetError(INVALID_SOCKET);
			return 0;
			}
			if(nlLockSocket(socket, NL_BOTH) == false)
			{
			return 0;
			}
			sock = nlSockets[socket];

			switch (name) {

			case PACKETS_SENT:
			result = sock->outstats.packets;
			break;

			case BYTES_SENT:
			result = sock->outstats.bytes;
			break;

			case AVE_BYTES_SENT:
			nlUpdateSocketOutStats(socket, 0, 0);
			result = sock->outstats.average;
			if(result == 0)
			{
				/* this corrects the stats for the first second */
				result = sock->outstats.curbytes;
			}
			break;

			case HIGH_BYTES_SENT:
			result = sock->outstats.highest;
			break;

			case PACKETS_RECEIVED:
			result = sock->instats.packets;
			break;

			case BYTES_RECEIVED:
			result = sock->instats.bytes;
			break;

			case AVE_BYTES_RECEIVED:
			nlUpdateSocketInStats(socket, 0, 0);
			result = sock->instats.average;
			if(result == 0)
			{
				/* this corrects the stats for the first second */
				result = sock->instats.curbytes;
			}
			break;

			case HIGH_BYTES_RECEIVED:
			result = sock->instats.highest;
			break;

			default:
			nlSetError(INVALID_ENUM);
			}
			nlUnlockSocket(socket, NL_BOTH);
			return result;
		}

		HL_EXP bool HL_APIENTRY nlClearSocketStat(NLsocket socket, NLenum name)
		{
			nl_socket_t	 *sock;

			if(nlIsValidSocket(socket) == false)
			{
			nlSetError(INVALID_SOCKET);
			return false;
			}
			if(nlLockSocket(socket, NL_BOTH) == false)
			{
			return false;
			}
			sock = nlSockets[socket];

			switch (name) {

			case PACKETS_SENT:
			sock->outstats.packets = 0;
			break;

			case BYTES_SENT:
			sock->outstats.bytes = 0;
			break;

			case AVE_BYTES_SENT:
			sock->outstats.average = 0;
			memset((int8_t *)sock->outstats.bucket, 0, sizeof(int32_t) * NL_NUM_BUCKETS);
			break;

			case HIGH_BYTES_SENT:
			sock->outstats.highest = 0;
			break;

			case PACKETS_RECEIVED:
			sock->instats.packets = 0;
			break;

			case BYTES_RECEIVED:
			sock->instats.bytes = 0;
			break;

			case AVE_BYTES_RECEIVED:
			sock->instats.average = 0;
			memset((int8_t *)sock->instats.bucket, 0, sizeof(int32_t) * NL_NUM_BUCKETS);
			break;

			case HIGH_BYTES_RECEIVED:
			sock->instats.highest = 0;
			break;

			case ALL_STATS:
			sock->outstats.packets = 0;
			sock->outstats.bytes = 0;
			sock->outstats.average = 0;
			memset((int8_t *)sock->outstats.bucket, 0, sizeof(int32_t) * NL_NUM_BUCKETS);
			sock->outstats.highest = 0;
			sock->instats.packets = 0;
			sock->instats.bytes = 0;
			sock->instats.average = 0;
			memset((int8_t *)sock->instats.bucket, 0, sizeof(int32_t) * NL_NUM_BUCKETS);
			sock->instats.highest = 0;
			break;

			default:
			nlSetError(INVALID_ENUM);
			nlUnlockSocket(socket, NL_BOTH);
			return false;
			}
			nlUnlockSocket(socket, NL_BOTH);
			return true;
		}

		HL_EXP uint16_t  HL_APIENTRY nlSwaps(uint16_t x)
		{
			if(NL_SWAP_TRUE)
			{
			return (uint16_t)(((((uint16_t)x) & 0x00ff) << 8) | ((((uint16_t)x) & 0xff00) >> 8));
			}
			else
			{
			return x;
			}
		}

		HL_EXP uint32_t   HL_APIENTRY nlSwapl(uint32_t x)
		{
			if(NL_SWAP_TRUE)
			{
			return (uint32_t)(((((uint32_t)x) & 0x000000ff) << 24) | ((((uint32_t)x) & 0x0000ff00) << 8) | ((((uint32_t)x) & 0x00ff0000) >> 8) | ((((uint32_t)x) & 0xff000000) >> 24));
			}
			else
			{
			return x;
			}
		}

		HL_EXP uint64_t   HL_APIENTRY nlSwapll(uint64_t x)
		{
			if(NL_SWAP_TRUE)
			{
			union {uint32_t l[2]; uint64_t x;} in, out;

			in.x = x;
			out.l[0] = nlSwapl(in.l[1]);
			out.l[1] = nlSwapl(in.l[0]);
			return out.x;
			}
			else
			{
			return x;
			}
		}

		HL_EXP float   HL_APIENTRY nlSwapf(float f)
		{
			if(NL_SWAP_TRUE)
			{
			uint32_t temp = (uint32_t)nlSwapl(*(uint32_t *)&f);

			return *((float *)&temp);
			}
			else
			{
			return f;
			}
		}

		HL_EXP double  HL_APIENTRY nlSwapd(double d)
		{
			if(NL_SWAP_TRUE)
			{
			union {uint32_t l[2]; double d;} in, out;

			in.d = d;
			out.l[0] = nlSwapl(in.l[1]);
			out.l[1] = nlSwapl(in.l[0]);
			return out.d;
			}
			else
			{
			return d;
			}
		}

		#if defined (__LCC__)
		BOOL WINAPI __declspec(dllexport) LibMain(/*@unused@*/HINSTANCE hinstDLL, /*@unused@*/DWORD fdwReason, /*@unused@*/LPVOID lpvReserved)
		{
			return TRUE;
		}
		#endif /* WINDOWS APP */
	}
}
