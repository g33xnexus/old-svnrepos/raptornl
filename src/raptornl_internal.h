/*
  HawkNL cross platform network library
  Copyright (C) 2000-2004 Phil Frisbie, Jr. (phil@hawksoft.com)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Library General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Library General Public License for more details.

  You should have received a copy of the GNU Library General Public
  License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
  Boston, MA  02111-1307, USA.

  Or go to http://www.gnu.org/copyleft/lgpl.html
*/

#ifndef INTERNAL_H
#define INTERNAL_H

#include "raptornl.h"
#include "hawkthreads.h"

/* for malloc and free */
#include <stdlib.h>

#ifndef MACOSX
#	include <malloc.h>
#endif

/* Windows CE does not have time.h functions */
#if defined (_WIN32_WCE)
	extern time_t time(time_t *timer);
#else
#	include <time.h>
#endif


#ifdef NL_LITTLE_ENDIAN
#	define NL_SWAP_TRUE (nlState.nl_big_endian_data == true)
#else
#	define NL_SWAP_TRUE (nlState.nl_big_endian_data != true)
#endif /* NL_LITTLE_ENDIAN */

#ifdef HL_WINDOWS_APP
	/* Windows systems */
#	ifdef _MSC_VER
#		pragma warning (disable:4201)
#		pragma warning (disable:4214)
#	endif /* _MSC_VER */

#	define WIN32_LEAN_AND_MEAN
#	include <windows.h>
#	include <tchar.h>

#	ifdef _MSC_VER
#		pragma warning (default:4201)
#		pragma warning (default:4214)
#	endif /* _MSC_VER */

#endif

/* part of portable unicode support */
#if !defined _TCHAR_DEFINED && !(defined _WCHAR_T_DEFINED && defined (__LCC__))
#	ifdef _UNICODE
#		define TEXT(x)	L##x
#		define _tcsncat	wcsncat
#		define _stprintf   swprintf
#		define _sntprintf  snwprintf
#		define _stscanf	swscanf
#		define _tcsncpy	wcsncpy
#		define _tcscspn	wcscspn
#		define _tcschr	 wcschr
#		define _tcslen	 wcslen
#		define _tcsrchr	wcsrchr
#		ifdef HL_WINDOWS_APP
#			define _ttoi	   _wtoi
#		else /* !HL_WINDOWS_APP*/
#			define _ttoi	   wtoi
#		endif /* !HL_WINDOWS_APP*/
#	else /* !UNICODE */
#		define TEXT(x)	x
#		define _tcsncat	strncat
#		define _stprintf   sprintf
#		define _sntprintf  snprintf
#		define _stscanf	sscanf
#		define _tcsncpy	strncpy
#		define _tcscspn	strcspn
#		define _tcschr	 strchr
#		define _tcslen	 strlen
#		define _ttoi	   atoi
#		define _tcsrchr	strrchr
#	endif /* !UNICODE */
#endif /* _INC_TCHAR */

/* internally for TCP packets and UDP connections, all data is big endian,
   so we force it so here using these macros */
#undef writeShort
#define writeShort(x, y, z)	 {*((uint16_t *)((int8_t *)&x[y])) = htons(z); y += 2;}
#undef readShort
#define readShort(x, y, z)	  {z = ntohs(*(uint16_t *)((int8_t *)&x[y])); y += 2;}

#define writeByte(x, y, z)	  (*(char *)&x[y++] = (char)z)
#define writeString(x, y, z)	{strcpy((char *)&x[y], (char *)z); y += (strlen((char *)z) + 1);}

#define NL_FIRST_GROUP		  (200000 + 1)

/* the minumum number of sockets that will be allocated */
#define NL_MIN_SOCKETS		  16

/* number of buckets for average bytes/second */
#define NL_NUM_BUCKETS		  8

/* number of packets stored for NL_LOOP_BACK */
#define NL_NUM_PACKETS		  8
#define NL_MAX_ACCEPT		   10

/* for nlLockSocket and nlUnlockSocket */
#define NL_READ				 0x0001
#define NL_WRITE				0x0002
#define NL_BOTH				 (NL_READ|NL_WRITE)

/* time in milliseconds that unreliable connect/accepts sleep while waiting */
#define NL_CONNECT_SLEEP	50

namespace RaptorNL
{
	namespace __PRIVATE__
	{
		/* the driver object */
		typedef struct
		{
			const NLchar /*@observer@*/*name;
			const NLchar /*@observer@*/*socktypes;
			NLenum	  type;
			bool   initialized;
			bool   (*Init)(void);
			void		(*Shutdown)(void);
			bool   (*Listen)(NLsocket socket);
			NLsocket	(*AcceptConnection)(NLsocket socket);
			NLsocket	(*Open)(uint16_t port, SocketType type);
			bool   (*Connect)(NLsocket socket, const NLaddress *address);
			void		(*Close)(NLsocket socket);
			int32_t	   (*Read)(NLsocket socket, /*@out@*/ void *buffer, int32_t nbytes);
			int32_t	   (*Write)(NLsocket socket, const void *buffer, int32_t nbytes);
			NLchar	  *(*AddrToString)(const NLaddress *address, /*@returned@*/ /*@out@*/ NLchar *string);
			bool   (*StringToAddr)(const NLchar *string, /*@out@*/ NLaddress *address);
			bool   (*GetLocalAddr)(NLsocket socket, /*@out@*/ NLaddress *address);
			NLaddress   *(*GetAllLocalAddr)(/*@out@*/ int32_t *count);
			bool   (*SetLocalAddr)(const NLaddress *address);
			NLchar	  *(*GetNameFromAddr)(const NLaddress *address, /*@returned@*/ /*@out@*/ NLchar *name);
			bool   (*GetNameFromAddrAsync)(const NLaddress *address, /*@out@*/ NLchar *name);
			bool   (*GetAddrFromName)(const NLchar *name, /*@out@*/ NLaddress *address);
			bool   (*GetAddrFromNameAsync)(const NLchar *name, /*@out@*/ NLaddress *address);
			bool   (*AddrCompare)(const NLaddress *address1, const NLaddress *address2);
			uint16_t	(*GetPortFromAddr)(const NLaddress *address);
			void		(*SetAddrPort)(NLaddress *address, uint16_t port);
			Error	   (*GetSystemError)(void);
			int32_t	   (*PollGroup)(int32_t group, NLenum name, /*@out@*/ NLsocket *sockets,
						int32_t number, int32_t timeout);
			bool   (*PollSocket)(NLsocket socket, NLenum name, int32_t timeout);
			bool   (*Hint)(NLenum name, int32_t arg);
			bool   (*SetSocketOpt)(NLsocket socket, NLenum name, int32_t arg);
			int32_t	   (*GetSocketOpt)(NLsocket socket, NLenum name);
		} nl_netdriver_t;

		typedef struct
		{
			int32_t	  bytes;		  /* bytes sent/received */
			int32_t	  packets;		/* packets sent/received */
			int32_t	  highest;		/* highest bytes/sec sent/received */
			int32_t	  average;		/* average bytes/sec sent/received */
			time_t	  stime;		  /* the last time stats were updated */
			int32_t	   lastbucket;	 /* the last bucket that was used */
			int32_t	  curbytes;	   /* current bytes sent/received */
			int32_t	  bucket[NL_NUM_BUCKETS];/* buckets for sent/received counts */
			bool   firstround;	 /* is this the first round through the buckets? */
		} nl_stats_t;

		typedef struct
		{
			/* info for NL_LOOP_BACK, NL_SERIAL, and NL_PARALLEL */
			int8_t	  *outpacket[NL_NUM_PACKETS];/* temp storage for packet data */
			int8_t	  *inpacket[NL_NUM_PACKETS];/* temp storage for packet data */
			int32_t	   outlen[NL_NUM_PACKETS];/* the length of each packet */
			int32_t	   inlen[NL_NUM_PACKETS];/* the length of each packet */
			int32_t	   nextoutused;	/* the next used packet */
			int32_t	   nextinused;	 /* the next used packet */
			int32_t	   nextoutfree;	/* the next free packet */
			int32_t	   nextinfree;	 /* the next free packet */
			NLsocket	accept[NL_MAX_ACCEPT];/* pending connects */
			NLsocket	consock;		/* the socket this socket is connected to */
		} nl_extra_t;

		/* the internal socket object */
		typedef struct
		{
			/* the current status of the socket */
			NLenum	  driver;		 /* the driver used with this socket */
			NLenum	  type;		   /* type of socket */
			bool   inuse;		  /* is in use */
			bool   connecting;	 /* a non-blocking TCP or UDP connection is in process */
			bool   conerror;	   /* an error occured on a UDP connect */
			bool   connected;	  /* is connected */
			bool   reliable;	   /* do we use reliable */
			bool   blocking;	   /* is set to blocking */
			bool   listen;		 /* can receive an incoming connection */
			bool   reuseaddr;	  /* can we reuse the address */
			bool   TCPNoDelay;	 /* disable Nagle on TCP */
			int32_t	   TTL;			/* TTL for multicast */
			int32_t	   realsocket;	 /* the real socket number */
			uint16_t	localport;	  /* local port number */
			uint16_t	remoteport;	 /* remote port number */
			NLaddress   addressin;	  /* address of remote system, same as the socket sockaddr_in structure */
			NLaddress   addressout;	 /* the multicast address set by nlConnect or the remote address for unconnected UDP */
			HTmutex	 readlock;	   /* socket is locked to update data */
			HTmutex	 writelock;	  /* socket is locked to update data */

			/* the current read/write statistics for the socket */
			nl_stats_t  instats;		/* stats for received */
			nl_stats_t  outstats;	   /* stats for sent */

			/* NL_RELIABLE_PACKETS info and storage */
			int8_t	  *outbuf;		/* temp storage for partially sent reliable packet data */
			int32_t	   outbuflen;	  /* the length of outbuf */
			int32_t	   sendlen;		/* how much still needs to be sent */
			int8_t	  *inbuf;		 /* temp storage for partially received reliable packet data */
			int32_t	   inbuflen;	   /* the length of inbuf */
			int32_t	   reclen;		 /* how much of the reliable packet we have received */
			bool   readable;	   /* a complete packet is in inbuf */
			bool   message_end;	/* a message end error ocured but was not yet reported */
			bool   packetsync;	 /* is the reliable packet stream in sync */
			/* pointer to extra info needed for NL_LOOP_BACK, NL_SERIAL, and NL_PARALLEL */
			nl_extra_t   *ext;
		} nl_socket_t;

		typedef struct
		{
			bool   socketStats;	/* enable collection of socket read/write statistics, default disabled */
			bool   nl_big_endian_data; /* is the packet data big endian? */
		} nl_state_t;

		/* used by the drivers to allocate and free socket objects */
		NLsocket nlGetNewSocket(void);

		/* other functions */
		bool nlGroupInit(void);
		void nlGroupShutdown(void);
		void nlGroupLock(void);
		void nlGroupUnlock(void);
		bool nlGroupGetSocketsINT(int32_t group, /*@out@*/ NLsocket *socket, /*@in@*/ int32_t *number);
		bool nlIsValidSocket(NLsocket socket);
		bool nlLockSocket(NLsocket socket, int32_t which);
		void nlUnlockSocket(NLsocket socket, int32_t which);
		void nlSetError(Error err);

		/* globals (as few as possible) */
		extern volatile nl_state_t nlState;

		typedef /*@only@*/ nl_socket_t *pnl_socket_t;
		extern /*@only@*/ pnl_socket_t *nlSockets;
	}
}

#endif /* INTERNAL_H */

