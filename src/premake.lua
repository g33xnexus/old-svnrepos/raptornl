package.name = "src"
package.kind = "dll"
package.language = "c++"
package.target = "RaptorNL"

package.libpaths = {}
package.includepaths = { "../include" }
package.buildflags = { "optimize", "no-frame-pointer", "extra-warnings", "no-rtti" }
package.defines = { "_REENTRANT" }

if windows then
	addoption ("with-pthread", "Use pthread instead of native windows threads.")
	addoption ("disable-builtin-pthread", "Use your own pthread build instead of the included binaries.")
	addoption ("enable-ipx", "Enable IPX support.")
end

addoption ("enable-loopback", "Enable loopback support.")
addoption ("enable-serial", "Enable serial support.")
addoption ("enable-modem", "Enable modem support.")
addoption ("enable-parallel", "Enable parallel support.")
addoption ("enable-safe-copy", "Use safe copying method on platforms which don't allow non-aligned memory access. (needed on Sparc and some WinCE devices)")
addoption ("enable-unicode", "Enable unicode support.")

if linux or bsd or macosx then --or solaris
	package.links = { "pthread" }
	package.buildoptions = { "-funroll-loops", "-ffast-math", "-Weffc++" }
	package.config["Debug"].buildoptions = { "-ggdb" }
	if macosx then
		table.insert (package.defines, "MACOSX")
		package.kind = "dylib"
		-- -r = relocatable?
		-- -Wl,-x = discard all local symbols
		package.linkoptions = { "-r", "-Wl,-x" }
	else
		table.insert (package.defines, "_GNU_SOURCE")
--		TODO: Add Solaris support to premake.
--		if solaris then
--			table.insert (package.defines, "NL_SAFE_COPY")
--			table.insert (package.defines, "SOLARIS")
--			table.insert (package.links, "nsl")
--			table.insert (package.links, "socket")
--		end
	end
else
	if windows then
		table.insert (package.defines, "WIN32")
		package.links = { "wsock32" }
		if options["with-pthread"] then
			table.insert (package.links, "pthread")
			if not options["disable-builtin-pthread"] then
				table.insert (package.libpaths, "pthread_win32")
				table.insert (package.includepaths, "pthread_win32")
			end
		end
		if options.target == "gnu" then
			package.buildoptions = { "-funroll-loops", "-ffast-math", "-Weffc++" }
		end
	end
end

if options["enable-ipx"] then
	table.insert (package.defines, "NL_INCLUDE_IPX")
end

if options["enable-loopback"] then
	table.insert (package.defines, "NL_INCLUDE_LOOPBACK")
end

if options["enable-serial"] then
	table.insert (package.defines, "NL_INCLUDE_SERIAL")
end

if options["enable-modem"] then
	table.insert (package.defines, "NL_INCLUDE_MODEM")
end

if options["enable-parallel"] then
	table.insert (package.defines, "NL_INCLUDE_PARALLEL")
end

if options["enable-safe-copy"] then
	table.insert (package.defines, "NL_SAFE_COPY")
end

if options["enable-unicode"] then
	table.insert (package.defines, "_UNICODE")
	table.insert (package.buildflags, "native-char")
	table.insert (package.buildflags, "unicode")
end


table.insert (package.links, "src_old")

table.insert (package.defines, "RNL_MAJOR_VERSION="..MAJOR_VERSION)
table.insert (package.defines, "RNL_MINOR_VERSION="..MINOR_VERSION)
table.insert (package.defines, "RNL_PATCH_VERSION="..PATCH_LEVEL)
table.insert (package.defines, "RNL_VERSION_STRING=\\\"RaptorNL "..MAJOR_VERSION.."."..MINOR_VERSION.."."..PATCH_LEVEL.."\\\"")

package.files = {
	"Address.cpp",
	"Buffer.cpp",
	"Socket.cpp",
	"ConnectSocket.cpp",
	"ListenSocket.cpp",
	"crc.cpp",
	"err.cpp",
	"errorstr.cpp",
	"group.cpp",
	"ipx.cpp",
	"loopback.cpp",
	"nl.cpp",
	"nltime.cpp",
	"sock.cpp"
}
