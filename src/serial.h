/*
  HawkNL cross platform network library
  Copyright (C) 2000-2004 Phil Frisbie, Jr. (phil@hawksoft.com)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Library General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Library General Public License for more details.

  You should have received a copy of the GNU Library General Public
  License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
  Boston, MA  02111-1307, USA.

  Or go to http://www.gnu.org/copyleft/lgpl.html
*/

#ifndef SERIAL_H
#define SERIAL_H

namespace RaptorNL
{
	namespace __PRIVATE__
	{
		bool serial_Init(void);
		void serial_Shutdown(void);
		bool serial_Listen(NLsocket socket);
		NLsocket serial_AcceptConnection(NLsocket socket);
		NLsocket serial_Open(uint16_t port, NLenum type);
		bool serial_Connect(NLsocket socket, NLaddress *address);
		bool serial_Close(NLsocket socket);
		int32_t serial_Read(NLsocket socket, void *buffer, int32_t nbytes);
		int32_t serial_Write(NLsocket socket, void *buffer, int32_t nbytes);
		int8_t *serial_AddrToString(NLaddress *address, int8_t *string);
		bool serial_StringToAddr(int8_t *string, NLaddress *address);
		bool serial_GetLocalAddr(NLsocket socket, NLaddress *address);
		bool serial_SetLocalAddr(NLaddress *address);
		int8_t *serial_GetNameFromAddr(NLaddress *address, int8_t *name);
		bool serial_GetNameFromAddrAsync(NLaddress *address, int8_t *name);
		bool serial_GetAddrFromName(int8_t *name, NLaddress *address);
		bool serial_GetAddrFromNameAsync(int8_t *name, NLaddress *address);
		bool serial_AddrCompare(NLaddress *address1, NLaddress *address2);
		uint16_t serial_GetPortFromAddr(NLaddress *address);
		void serial_SetAddrPort(NLaddress *address, uint16_t port);
		int32_t serial_GetSystemError(void);
		int32_t serial_PollGroup(int32_t group, NLenum name, NLsocket *sockets, int32_t number, int32_t timeout);
		bool serial_PollSocket(NLsocket socket, NLenum name, int32_t timeout);
		bool serial_Hint(NLenum name, int32_t arg);
		bool serial_SetSocketOpt(NLsocket socket, NLenum name, int32_t arg);
		int32_t serial_GetSocketOpt(NLsocket socket, NLenum name);
	}
}

#endif /* SERIAL_H */

