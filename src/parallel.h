/*
  HawkNL cross platform network library
  Copyright (C) 2000-2004 Phil Frisbie, Jr. (phil@hawksoft.com)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Library General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Library General Public License for more details.

  You should have received a copy of the GNU Library General Public
  License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
  Boston, MA  02111-1307, USA.

  Or go to http://www.gnu.org/copyleft/lgpl.html
*/

#ifndef PARALLEL_H
#define PARALLEL_H

namespace RaptorNL
{
	namespace __PRIVATE__
	{
		bool parallel_Init(void);
		void parallel_Shutdown(void);
		bool parallel_Listen(NLsocket socket);
		NLsocket parallel_AcceptConnection(NLsocket socket);
		NLsocket parallel_Open(uint16_t port, NLenum type);
		bool parallel_Connect(NLsocket socket, NLaddress *address);
		bool parallel_Close(NLsocket socket);
		int32_t parallel_Read(NLsocket socket, void *buffer, int32_t nbytes);
		int32_t parallel_Write(NLsocket socket, void *buffer, int32_t nbytes);
		int8_t *parallel_AddrToString(NLaddress *address, int8_t *string);
		bool parallel_StringToAddr(int8_t *string, NLaddress *address);
		bool parallel_GetLocalAddr(NLsocket socket, NLaddress *address);
		bool parallel_SetLocalAddr(NLaddress *address);
		int8_t *parallel_GetNameFromAddr(NLaddress *address, int8_t *name);
		bool parallel_GetNameFromAddrAsync(NLaddress *address, int8_t *name);
		bool parallel_GetAddrFromName(int8_t *name, NLaddress *address);
		bool parallel_GetAddrFromNameAsync(int8_t *name, NLaddress *address);
		bool parallel_AddrCompare(NLaddress *address1, NLaddress *address2);
		uint16_t parallel_GetPortFromAddr(NLaddress *address);
		void parallel_SetAddrPort(NLaddress *address, uint16_t port);
		int32_t parallel_GetSystemError(void);
		int32_t parallel_PollGroup(int32_t group, NLenum name, NLsocket *sockets, int32_t number, int32_t timeout);
		bool parallel_PollSocket(NLsocket socket, NLenum name, int32_t timeout);
		bool parallel_Hint(NLenum name, int32_t arg);
		bool parallel_SetSocketOpt(NLsocket socket, NLenum name, int32_t arg);
		int32_t parallel_GetSocketOpt(NLsocket socket, NLenum name);
	}
}

#endif /* PARALLEL_H */

