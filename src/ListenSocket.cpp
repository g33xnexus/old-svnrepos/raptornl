/**
 * @file ListenSocket.cpp
 * @brief Implementation of the ListenSocket class.
 *
 * @author David Bronke
 *
 * This header provides the implementation of the ListenSocket class.
 */

#include "ListenSocket.h"

namespace RaptorNL
{
	////////////////////////////////////////////////////////////////////////////
	ListenSocket::ListenSocket (uint16_t port, SocketType type)
	{
		socket_ = __PRIVATE__::nlOpen (port, type);
		if (socket_ != NL_INVALID)
		{
			if (!__PRIVATE__::nlListen (socket_))
			{
				__PRIVATE__::nlClose (socket_);
				socket_ = NL_INVALID;
			} // end if
		} // end if
	} // end ListenSocket ()


	////////////////////////////////////////////////////////////////////////////
	ListenSocket::~ListenSocket ()
	{
	} // end ~ListenSocket ()


	////////////////////////////////////////////////////////////////////////////
	Socket* ListenSocket::acceptConnection ()
	{
		NLsocket sock = __PRIVATE__::nlAcceptConnection (socket_);
		if (sock != NL_INVALID)
		{
			return new Socket (sock);
		}
		else
		{
			return NULL;
		}
	} // end acceptConnection ()
} // end namespace RaptorNL

