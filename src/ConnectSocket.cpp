/**
 * @file ConnectSocket.cpp
 * @brief Implementation of the ConnectSocket class.
 *
 * @author David Bronke
 *
 * This header provides the implementation of the ConnectSocket class.
 */

#include "ConnectSocket.h"
#include "Address.h"

namespace RaptorNL
{
	////////////////////////////////////////////////////////////////////////////
	ConnectSocket::ConnectSocket (uint16_t port, SocketType type, Address* address)
	{
		socket_ = __PRIVATE__::nlOpen (port, type);
		if (socket_ != NL_INVALID)
		{
			if (!__PRIVATE__::nlConnect (socket_, address->getAddress ()))
			{
				__PRIVATE__::nlClose (socket_);
				socket_ = NL_INVALID;
			} // end if
		} // end if
	} // end ConnectSocket ()


	////////////////////////////////////////////////////////////////////////////
	ConnectSocket::~ConnectSocket ()
	{
	} // end ~ConnectSocket ()
} // end namespace RaptorNL

