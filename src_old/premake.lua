package.name = "src_old"
package.kind = "lib"
package.language = "c++"
package.target = "RaptorNLLegacy"

package.libpaths = {}
package.includepaths = { "../include" }
package.buildflags = { "optimize", "no-frame-pointer", "extra-warnings", "no-rtti" }

if windows then
	addoption("with-pthread", "Use pthread instead of native windows threads.")
	addoption("disable-builtin-pthread", "Use your own pthread build instead of the included binaries.")
end

if linux or bsd or macosx then --or solaris
	package.links = { "pthread" }
	if macosx then
		package.defines = { "MACOSX", "_REENTRANT" }
		-- TODO: Does premake already do -dylib under Mac OS X?
		package.linkoptions = { "-dylib -r -Wl,-x" }
	else
--		TODO: Bug the Premake author to put in Solaris support.
--		if solaris then
--			package.defines = { "_GNU_SOURCE", "_REENTRANT", "NL_SAFE_COPY", "SOLARIS" }
--			package.links = { "pthread", "nsl", "socket" }
--		else
			package.defines = { "_GNU_SOURCE", "_REENTRANT" }
--		end
	end
	package.buildoptions = { "-fPIC -funroll-all-loops -ffast-math" }
else
	if windows then
		package.defines = { "WIN32", "_REENTRANT" }
		if options["with-pthread"] then
			package.links = { "pthread", "wsock32" }
			if not options["disable-builtin-pthread"] then
				table.insert (package.libpaths, "pthread_win32")
				table.insert (package.includepaths, "pthread_win32")
			end
		else
			package.links = { "wsock32" }
		end
		if options.target == "gnu" then
			package.linkoptions = { "-shared" }
			package.buildoptions = { "-funroll-all-loops -ffast-math" }
		end
	end
end

package.files = {
	"htcondition.c",
	"hthread.c",
	"htmutex.c"
}
