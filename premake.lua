project.name = "RaptorNL"

MAJOR_VERSION = 0
MINOR_VERSION = 0
PATCH_LEVEL = 0

dopackage ("src_old")
dopackage ("src")
dopackage ("samples/simpChat")

addoption ("install", "Install RaptorNL into the given prefix.")

function doinstall (cmd, prefix)
	if (not prefix) then
		print ('No prefix specified. Installing to default path.')
		prefix = "/usr/local"
	end
	function inst (file, dest)
		print (file .. " => " .. dest)
		os.copyfile (file, prefix .. '/' .. dest .. '/' .. path.getname (file))
	end

	print ('Installing to: ' .. prefix)
	os.copyfile (project.packages["src"].target, prefix .. '/bin/' .. MAJOR_VERSION .. "." .. MINOR_VERSION .. "." .. PATCH_LEVEL)
	inst (project.packages["samples/simpChat"].target, 'bin')
end
